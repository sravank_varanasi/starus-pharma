<?php include('header.php'); ?>

<section class="inner-banner-section fadeIn wow">
    <div class="solid-bg">
      <div class="container">
        <div class="banner-title fadeInDown wow" data-wow-delay=".2s">
          <h1>We Care About Your Life</h1>
          <p>To inspire hope and contribute to health and well-being by providing 
              the best care to every patient.</p>
          <div class="banner-breadcrumbs"> <span><a href="<?php echo $CONFIG_SERVER_ROOT;?>" class="home"><span>Home</span></a></span> <span class="sep">/</span> <span class="current">Gallery</span> </div>
        </div>
      </div>
    </div>
  </section>
  <section class="portfolio-section common-inner-block">
    <div class="container">
        <div class="row">
            <div class="col-md-12  section-title text-center fadeInDown wow" data-wow-delay=".4s" style=" margin-bottom:0px;">
            <h3 class="sectitle">Gallery</h3>
             </div>
        </div>
        <div class="row">
        <div class="col-md-3">
                  <div class="thumbnail-box">
                        <a id="fancybox-manual-bglr" href="javascript:;">
                                <figure>
                                  <img src="images/gallery/cyclemeetingjuly/1.jpg" class="img-responsive">
                                </figure>
                  
                              </a>
                              <div class="caption">
                                    <h4 class="entry-title"><a href="#" rel="bookmark">Cycle Meeting- July 2018</a></h4>
                                    <p>Location - Bangalore</p>
                              </div>

                  </div>
              </div>
              
            <div class="col-md-3">
                  <div class="thumbnail-box">
                        <a id="fancybox-manual-mumbai" href="javascript:;">
                                <figure>
                                  <img src="images/gallery/budget-2018/IMG_6820.JPG" class="img-responsive">
                                </figure>
                  
                              </a>
                              <div class="caption">
                                    <h4 class="entry-title"><a href="#" rel="bookmark">Budget Meeting 2018</a></h4>
                                    <p>Location - Mumbai</p>
                              </div>

                  </div>
              </div>

                

      </div>
       
        
            

    </div>
  </section>
  
  
    <?php include('footer.php'); ?>
  