<?php include('header.php'); ?>
<section class="inner-banner-section fadeIn wow">
    <div class="solid-bg">
      <div class="container">
        <div class="banner-title fadeInDown wow" data-wow-delay=".2s">
          <h1>We Care About Your Life</h1>
          <p>To inspire hope and contribute to health and well-being by providing 
              the best care to every patient.</p>
          <div class="banner-breadcrumbs"> <span><a href="<?php echo $CONFIG_SERVER_ROOT;?>" class="home"><span>Home</span></a></span> <span class="sep">/</span> <span class="current">About</span> </div>
        </div>
      </div>
    </div>
  </section>
  <section class="common-inner-block contact-block">
    <div class="container">
    <div class="row">
       <div class="col-md-12 section-title fadeInUp wow" data-wow-delay=".2s">
          <h5 class="subtitle fadeIn wow" data-wow-delay=".3s">About</h5>                                    
          <h3 class="sectitle">What We Are</h3>
        </div>                                    
     </div>
     <div class="row">
        <div class="col-md-4 fadeInUp wow">
          <div class="icon-box text-center"> <img src="images/icon1.png">
            <h4><a href="#">Our Values</a></h4>
            <ul>
              <li>Innovation</li>
              <li>Performance Focus</li>
              <li>Customer centricity</li>
              <li>Care for Society</li>
              <li>Ownership and Collaboration</li>
          </ul>

          </div>
        </div>
        <div class="col-md-4 fadeInUp wow" data-wow-delay=".5s">
          <div class="icon-box"> <img src="images/icon2.png">
            <h4><a href="#">Our Mission</a></h4>
            <p>To inspire hope and contribute to health and well-being by providing the best care to every patient.</p>
          </div>
        </div>
        <div class="col-md-4 fadeInUp wow" data-wow-delay=".7s">
          <div class="icon-box"> <img src="images/icon3.png">
            <h4><a href="#">Our Vision</a></h4>
            <p>To continually set new benchmarks in healthcare with power of human expertise, innovative molecules and advanced technology.</p>
          </div>
        </div>
        </div>
     
    </div>
  </section>
<section class="about-contcnt">
  <div class="container">
  <div class="row">
        <div class="col-md-12 fadeInUp wow" data-wow-delay=".2s">
        <p class="">Starus Pharmaceuticals Private Limited is a professionally managed progressive pharmaceutical marketing company headquartered at Hyderabad, India, incorporated under Ministry of Corporate Affairs, Govt of India wide Regn No: engaged in improving people’s lives through meaningful and innovative therapeutic solutions for various disease ailments.</p>
        <p>The company is currently involved in marketing of registered therapeutic solutions with wide range of choices in the field of Neurology, Psychiatry, Gynaecology, Optholmology, Gastro and Ortho segments.</p>
        <p>At Starus, we are guided by the understanding that, there is a patient at the centre of everything what we do. By pioneering new solutions that improve and expand care, we are dedicated to create the ideal experience for all the patients suffering from various disease ailments.</p>
        <p>We harness the power of clinical information by providing clinicians and health care providers with real-time information for more confident decision making and efficient workflow.</p>
        <p>At Starus, we focus on delivering most technologically appropriate innovative products and solutions for clinicians to treat and manage many of today’s most prevalent chronic diseases by understanding therapy and treatment gaps.</p>
        <p>These commitments are the driving force behind our planned investments in products with new drug delivery systems.</p>
        <p>Starus Pharma’s success and incessant growth lies in clinical and marketing execution of strategic moves made in the areas of launching innovative molecules and end user focused approach. Starus currently progressing at over 100% growth over last year and expanding into other parts of India.</p>
      </div>
    </div>
  </div>
</section>
<section class="team-section">
    <div class="container">
      <div class="row">
        <div class="col-md-12 section-title fadeInUp wow" data-wow-delay=".2s">
          <h5 class="subtitle">Team</h5>
          <h3 class="sectitle">Board of Directors</h3>
          <h6>Our practice prides itself on providing the very finest level of service, from a simple filling to complex cosmetic dentistry.</h6>
          
        </div>
      </div>
      <div class="row team-members-section text-center">
        <div class="col-md-3 fadeInUp wow center-block" data-wow-delay=".2s">
          <div class="team-member">
            <div class="member-img"> <img src="images/2-1-5-2-1.jpg" alt="Udaya Kiran Perla">
              <div class="profail">
                <div class="content">
                  <h3>Udaya Kiran Perla</h3>
                  <p class="designation">Managing Director</p>
                  <p>Mr Udaya Kiran is the founder of Starus Pharmaceuticals Pvt Ltd., and has extensive experience in sales and marketing in the Pharmaceutical Industry. </p>
                  <a href="udaya-kiran.php" class="btn readmore"> Read more</a> </div>
              </div>
            </div>
            <div class="name">
            <h3>Udaya Kiran Perla</h3>
            <p>Managing Director</p>
            </div>
          </div>
        </div>
        <div class="col-md-3 fadeInUp wow center-block" data-wow-delay=".4s">
          <div class="team-member">
            <div class="member-img"> <img src="images/2-1-5-2-1.jpg" alt="M Ramesh Kumar">
              <div class="profail">
                <div class="content">
                  <h3>M Ramesh Kumar</h3>
                  <p class="designation">Managing Director</p>
                  <p>Mr Ramesh Kumar, has extensive experience of 3 decades in sales and marketing with India’s leading pharmaceutical companies including Gufic</p>
                  <a href="ramesh-kumar.php" class="btn readmore"> Read more</a> </div>
              </div>
            </div>
            <div class="name">
              <h3>M Ramesh Kumar</h3>
              <p>Managing Director</p>
            </div>
          </div>
        </div>
        
        
      </div>
    </div>
  </section>

<?php include('footer.php'); ?>