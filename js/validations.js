    $(document).ready(function() {
		$.validator.addMethod(
            'defaultCheck', function (value, element) { 
                if (element.value == element.alt || element.value == "") {
                    return false;
                }
                return true;
		  },'PLease'); 

		
			//this below method for allow only letters validation
			$.validator.addMethod("lettersOnly", function(value, element) { 
				return this.optional(element) || /^[a-z\s]+$/i.test(value);
			}, "Please Enter Only letters"); 
			//this below method for allow letters numbers and spaces validation
			$.validator.addMethod("alphaNumeric", function(value, element) { 
				return this.optional(element) || /^[a-z0-9A-Z \s]+$/i.test(value);
			}, "Please Enter Only Letters and Numbers"); 
			//this below method for allow letters numbers and spaces and hypen validation
			$.validator.addMethod("alphaNumerichypen", function(value, element) { 
				return this.optional(element) || /^[a-z0-9A-Z- \s]+$/i.test(value);
			}, "Please Enter Only Letters and Numbers"); 
			//this below method for allow only phone validation
			$.validator.addMethod("numbersOnly", function(value, element) { 
				return this.optional(element) || /^\+?[0-9-]+$/i.test(value);
			}, "Please Enter Only Valid Contact");
			//this below method for allow only number validation
			$.validator.addMethod("integerOnly", function(value, element) { 
				return this.optional(element) || /^\d+$/i.test(value);
			}, "Please Enter Only number");
		
		//This below block of the code for Contact Form
		$("#contactForm").validate({
			onkeyup: function(element) { $(element).valid(); },
			rules:{
				first_name: {required: true,alphaNumerichypen:true,minlength:2},
				last_name : {required: true,alphaNumerichypen:true,minlength:2},
				email_address: {required: true,email: true},
				contact_no: {required: true,numbersOnly:true},
				message: {required: true}
			},
			messages:{
				first_name: {required:"This is a required field."},
				last_name: {required:"This is a required field."},
				email_address: {required:"This is a required field.",required:"Please Enter A Valid Email Address."},
				contact_no: {required:"This is a required field.",required:"Please Enter A Valid Contact Number."},
				message: {required:"This is a required field."},
			},
			submitHandler: function(form) {
				$("#contactForm .Submit").attr("disabled", true);
				form.submit();
			}, 
		});
		//This above block of the code for Contact Form
		//This below block of the code for Careers Form
		$("#careersForm").validate({
			onkeyup: function(element) { $(element).valid(); },
			rules:{
				name: {required: true,alphaNumerichypen:true,minlength:2},
				email: {required: true,email: true},
				phone: {required: true,numbersOnly:true},
				total_exp: {required: true,integerOnly:true},
				resume: {required: true},
			},
			messages:{
				name: {required:"This is a required field."},
				email: {required:"This is a required field.",required:"Please Enter A Valid Email Address."},
				phone: {required:"This is a required field."},
				total_exp: {required:"This is a required field."},
				resume: {required:"This is a required field."},
			},
			submitHandler: function(form) {
				$("#careersForm .submit").attr("disabled", true);
				form.submit();
			}, 
		});
		//This above block of the code for Careers Form
    });