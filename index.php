<?php include('header.php'); ?>
<section class="banner-section">
    <div id="bootstrap-touch-slider" class="carousel bs-slider fade  control-round indicators-line" data-ride="carousel" data-pause="hover" data-interval="false" > 
      
    
      
      <!-- Wrapper For Slides -->
      <div class="carousel-inner" role="listbox"> 
        
        <!-- first Slide -->
        <div class="item active"> 
          
          <!-- Slide Background --> 
          <img src="images/banner1.jpg" alt="Evaluation of the Current Safety"  class="slide-image"/>
          <!-- <div class="bs-slider-overlay"></div> -->
          <div class="container">
            <div class="row"> 
              <!-- Slide Text Layer -->
              <div class="slide-text slide_style_left slide-width-70p">

                <h2 data-animation="animated zoomInRight">We Care About Your Life</h2>
                <p data-animation="animated fadeInLeft">To inspire hope and contribute to health and well-being by providing 
                    the best care to every patient.</p>
                <a href="/contact" class="btn btn-default cust-btn-outline" data-animation="animated fadeInLeft">Contact Us</a> 
               </div>
            </div>
          </div>
        </div>
        <!-- End of Slide --> 
        
        
       
      
      </div>
      <!-- End of Wrapper For Slides --> 
      
      <!-- Left Control --> 
      <a class="left carousel-control" href="#bootstrap-touch-slider" role="button" data-slide="prev"> <span class="fa fa-angle-left" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> 
      
      <!-- Right Control --> 
      <a class="right carousel-control" href="#bootstrap-touch-slider" role="button" data-slide="next"> <span class="fa fa-angle-right" aria-hidden="true"></span> <span class="sr-only">Next</span> </a> </div>
  </section>
 
<section class="services-block gray-bg-block white-bg-block">
    <div class="container">
      <div class="row">
        <div class="col-md-12 section-title fadeInUp wow" data-wow-delay=".2s">
            <h5 class="subtitle">WELCOME</h5>
          <h3 class="sectitle">About Us</h3>
          <p class="subline">Starus Pharmaceuticals Private Limited is a professionally managed progressive pharmaceutical marketing company headquartered at Hyderabad, India, incorporated under Ministry of Corporate Affairs, Govt of India wide Regn No: engaged in improving people’s lives through meaningful and innovative therapeutic solutions for various disease ailments.</p>
        </div>
      </div>
     
      <a href="<?=$CONFIG_SERVER_ROOT.'about'?>" class="icon-more"> <img src="images/more-icon-new.png"> </a>    
    </div>
  </section>

  <section class="whychoose-block">
      <div class="container">
          <div class="row">
              <div class="col-md-12 section-title fadeInUp wow" data-wow-delay=".2s">
                  <h5 class="subtitle">OUR VALUES</h5>
                  <h3 class="sectitle">Why Choose Us</h3>
              
              </div>
          </div>
          <div class="row">
              <div class="col-md-4 fadeInUp wow" data-wow-delay=".4s">
                <div class="feature-box">
                    <div class="icon"><img src="images/innovation-icon.png"></div>
                    <div class="info-block"> <h3>INNOVATION</h3>
                      <p>Create an environment that fosters creativity and openness to new ideas leading to advancement of business process.</p>
                  </div>
                    </div>
              </div>
              <div class="col-md-4 fadeInUp wow" data-wow-delay=".6s">
                  <div class="feature-box">
                      <div class="icon"><img src="images/per-icon.png"></div>
                      <div class="info-block"> <h3>PERFORMANCE FOCUS</h3>
                        <p>Determined to achieve our business objectives and explore opportunities for accelerating growth.</p>
                    </div>
                      </div>
                </div>
                <div class="col-md-4 fadeInUp wow" data-wow-delay=".8s">
                    <div class="feature-box">
                        <div class="icon"><img src="images/cust-icon.png"></div>
                        <div class="info-block"> <h3>CUSTOMER CENTRICITY</h3>
                          <p>Achieve excellence in our products and services to exceed customer expectations and build long term relationships.</p>
                      </div>
                        </div>
                  </div>
                  <div class="col-md-4 fadeInUp wow" data-wow-delay="1s">
                      <div class="feature-box">
                          <div class="icon"><img src="images/care-icon.png"></div>
                          <div class="info-block"> <h3>CUSTOMER CENTRICITY</h3>
                            <p>Achieve excellence in our products and services to exceed customer expectations and build long term relationships.</p>
                        </div>
                          </div>
                    </div>
                    <div class="col-md-4 fadeInUp wow" data-wow-delay="1.2s">
                        <div class="feature-box">
                            <div class="icon"><img src="images/own-icon.png"></div>
                            <div class="info-block"> <h3>CUSTOMER CENTRICITY</h3>
                              <p>Achieve excellence in our products and services to exceed customer expectations and build long term relationships.</p>
                          </div>
                            </div>
                      </div>
                      <div class="col-md-4 text-center fadeInUp wow" data-wow-delay="1.4s">
                          <div class="feature-box blue-box">
                              <img src="images/more-icon.png">
                              <a href="<?=$CONFIG_SERVER_ROOT.'about'?>"><h5>And Many More ...</h5></a>
                              </div>
                        </div>
          </div>
      </div>
  </section>
 <section class="future-prod">
    <div class="container">
        <div class="row">
            <div class="col-md-12 section-title fadeInUp wow" data-wow-delay=".2s">
                <h5 class="subtitle">OUR Portfolio</h5>
                <h3 class="sectitle">Featured Products </h3>
                <p>Our practice prides itself on providing the very finest level of products, from a simple to complex formulae.</p>
            
            </div> 
        </div>
        <div class="row">
          <div class="col-md-12 text-center product-list fadeInUp wow" data-wow-delay=".4s">
              <ul>
                  <li><img src="images/product-1.jpg"></li> 
                  <li><img src="images/product-2.jpg"></li>
                  <li><img src="images/product-3.jpg"></li>
                  <li><img src="images/product-4.jpg"></li>
              </ul>
          </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center fadeInUp wow"  data-wow-delay=".6s">
                <a href="<?=$CONFIG_SERVER_ROOT.'products'?>" class="btn btn-default"> View All Products </a>
              </div>
        </div>
    </div>
 </section>
 <section class="action-band">
   <div class="container">
        <div class="row call-to-action fadeInUp wow">
            <div class="col-md-9">
               
                <p>We are proud to have the opportunity to 
                    give you the Health of your dreams.</p>
            </div>
            <div class="col-md-3">
                <a href="#" class="bnt btn-primary cust-btn-outline">Contact Us Now !</a>
            </div>
        </div>
   </div>

 </section>
  

 <?php include('footer.php'); ?>