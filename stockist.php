<?php include('header.php'); ?>
<section class="inner-banner-section fadeIn wow">
    <div class="solid-bg">
      <div class="container">
        <div class="banner-title fadeInUp wow" data-wow-delay=".2s">
          <h1>We Care About Your Life</h1>
          <p>To inspire hope and contribute to health and well-being by providing 
              the best care to every patient.</p>
          <div class="banner-breadcrumbs"> <span><a href="<?php echo $CONFIG_SERVER_ROOT;?>" class="home"><span>Home</span></a></span> <span class="sep">/</span> <span class="current">Stockist</span> </div>
        </div>
      </div>
    </div>
  </section>
<section class="common-inner-block">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
             <p class="subtitle">Following are the area wise details of our stockists.</p>  
            </div>
        </div>
        <div class="row custm-tab-nav">
            <div class="col-md-12">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#telangana" aria-controls="telangana" role="tab" data-toggle="tab">Telangana</a></li>
                    <li role="presentation"><a href="#andhra_pradesh" aria-controls="andhra_pradesh" role="tab" data-toggle="tab">Andhra Pradesh
                    </a></li>
                    <li role="presentation"><a href="#karnataka" aria-controls="karnataka" role="tab" data-toggle="tab">Karnataka</a></li>
                   
                </ul>
                
                <!-- Tab panes -->
                <div class="tab-content ui-block-contaciner">
                    <div role="tabpanel" class="tab-pane " id="andhra_pradesh">

                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        TIRUPATHI
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="panel-body">
                                            <ul class="list-of">
                                                <li>
                                                    <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                   <div class="info">
                                                       <h4>RAYALASEEMA DRUG HOUSE </h4>
                                                    <span>Tirupathi</span>
                                                    <h3><span class="lbl">Person:</span>Damodar Naidu</h3>
                                                    <h5><span class="lbl">Mobile:</span>9347775477, 0877-2284024</h5></div> 
                                                </li>

                                                <li>
                                                        <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                       <div class="info">
                                                           <h4>TANUJA MEDICAL AGENCIES</h4>
                                                        <span>Tirupathi</span>
                                                        <h3><span class="lbl">Person:</span>Madhav</h3>
                                                        <h5><span class="lbl">Mobile:</span>9951888877</h5></div> 
                                                    </li>

                                                    <li>
                                                            <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                           <div class="info">
                                                               <h4>RITHIKA MEDICAL AGENCIES</h4>
                                                            <span>Tirupathi</span>
                                                            <h3><span class="lbl">Person:</span>Mahesh Gupta</h3>
                                                            <h5><span class="lbl">Mobile:</span>9848303132</h5></div> 
                                                        </li>
                                                        <li>
                                                                <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                               <div class="info">
                                                                   <h4>PRASHANTH MEDICAL DISTRIBUTORS</h4>
                                                                <span>Tirupathi</span>
                                                                <h3><span class="lbl">Person:</span>Nagaraju</h3>
                                                                <h5><span class="lbl">Mobile:</span>8501898247</h5></div> 
                                                            </li>
                                                        
                                            </ul> 
                                    </div>
                                </div>
                            </div>
                    
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingTwo">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                           
                                        ANANTHAPUR
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                    <div class="panel-body">
                                            <ul class="list-of">
                                                    <li>
                                                        <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                       <div class="info">
                                                           <h4>NEW GALAXY MEDICAL AGENCIES</h4>
                                                        <span>Ananthapur</span>
                                                        <h3><span class="lbl">Person:</span>Maruthi</h3>
                                                        <h5><span class="lbl">Mobile:</span>9966919133</h5></div> 
                                                    </li>
    
                                                    <li>
                                                            <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                           <div class="info">
                                                               <h4>MOUNIKA MEDICAL AGENCIES</h4>
                                                            <span>Ananthapur</span>
                                                            <h3><span class="lbl">Person:</span>Abhi</h3>
                                                            <h5><span class="lbl">Mobile:</span>8897161742</h5></div> 
                                                        </li>
    
                                                      
                                                </ul> 
                                    </div>
                                </div>
                            </div>
                    
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingThree">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                          
                                        KADAPA
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                    <div class="panel-body">
                                            <ul class="list-of">
                                                    <li>
                                                        <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                       <div class="info">
                                                           <h4>MADHU MEDICAL AGENCIES</h4>
                                                        <span>Kadapa</span>
                                                        <h3><span class="lbl">Person:</span>-</h3>
                                                        <h5><span class="lbl">Mobile:</span>-</h5></div> 
                                                    </li>
    
                                                    <li>
                                                            <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                           <div class="info">
                                                               <h4>K.B.MEDICAL DISTRIBUTORS</h4>
                                                            <span>Kadapa</span>
                                                            <h3><span class="lbl">Person:</span>Teja</h3>
                                                            <h5><span class="lbl">Mobile:</span>9985226022</h5></div> 
                                                        </li>
    
                                                    
                                                </ul> 
                                    </div>
                                </div>
                            </div>
                    
                      


                        <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="heading4">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false" aria-controls="collapse4">
                                          
                                        KURNOOL
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
                                    <div class="panel-body">
                                            <ul class="list-of">
                                                
                                            <li>
                                                            <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                           <div class="info">
                                                               <h4>AROGYA MEDICAL AGENCIES</h4>
                                                            <span>Kurnool</span>
                                                            <h3><span class="lbl">Person:</span>Venkat Reddy</h3>
                                                            <h5><span class="lbl">Mobile:</span>9010004037</h5></div> 
                                                        </li>
                                                    <li>
                                                        <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                       <div class="info">
                                                           <h4>SRINIVASA MEDICAL AGENCIES</h4>
                                                        <span>Kurnool</span>
                                                        <h3><span class="lbl">Person:</span>-</h3>
                                                        <h5><span class="lbl">Mobile:</span>-</h5></div> 
                                                    </li>
    
                                                    <li>
                                                            <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                           <div class="info">
                                                               <h4>R.R.MEDICAL &VETENERY</h4>
                                                            <span>Kurnool</span>
                                                            <h3><span class="lbl">Person:</span>Teja</h3>
                                                            <h5><span class="lbl">Mobile:</span>9985226022</h5></div> 
                                                        </li>
    
                                                    
                                                </ul> 
                                    </div>
                                </div>
                            </div>


                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="heading5">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse5" aria-expanded="false" aria-controls="collapse5">
                                          
                                        NELLORE
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading5">
                                    <div class="panel-body">
                                            <ul class="list-of">
                                                
                                            <li>
                                                            <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                           <div class="info">
                                                               <h4>SHREE NIDHI MEDICAL CORPORATION</h4>
                                                            <span>J.V.Street, Near SBI, Nellore</span>
                                                            <h3><span class="lbl">Person:</span>Mohan</h3>
                                                            <h5><span class="lbl">Mobile:</span>9392292212, 0885500228</h5></div> 
                                                        </li>
                                                    <li>
                                                        <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                       <div class="info">
                                                           <h4>DEVI MEDICAL CORPORATION</h4>
                                                        <span>Tunk Road, Nellore</span>
                                                        <h3><span class="lbl">Person:</span>-</h3>
                                                        <h5><span class="lbl">Mobile:</span>9533633533</h5></div> 
                                                    </li>
    
                                                    <li>
                                                            <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                           <div class="info">
                                                               <h4>BABA MEDICAL AGENCIES </h4>
                                                            <span>Nellore   </span>
                                                            <h3><span class="lbl">Person:</span>-</h3>
                                                            <h5><span class="lbl">Mobile:</span>-</h5></div> 
                                                        </li>

                                                        <li>
                                                            <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                           <div class="info">
                                                               <h4>LAKSHMI SRIDHARA MEDICALS</h4>
                                                            <span>Trunk Raod, Nellore  </span>
                                                            <h3><span class="lbl">Person:</span>Subba rao</h3>
                                                            <h5><span class="lbl">Mobile:</span>0861-2324991</h5></div> 
                                                        </li>
    
                                                    
                                                </ul> 
                                    </div>
                                </div>
                            </div>
                    

                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="heading6">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse6" aria-expanded="false" aria-controls="collapse6">
                                          
                                        ONGOLE
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading6">
                                    <div class="panel-body">
                                            <ul class="list-of">
                                                
                                            <li>
                                                            <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                           <div class="info">
                                                               <h4>KAMAKSHI MEDICAL FORUM</h4>
                                                            <span>Gold Shops Road, Ongole</span>
                                                            <h3><span class="lbl">Person:</span>M.Bramaiah</h3>
                                                            <h5><span class="lbl">Mobile:</span>9949493793</h5></div> 
                                                        </li>
                                                    <li>
                                                        <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                       <div class="info">
                                                           <h4>BHAVANI MEDICAL AGENCIES </h4>
                                                        <span>Opp HP Petrol Bunk</span>
                                                        <h3><span class="lbl">Person:</span>Madhu</h3>
                                                        <h5><span class="lbl">Mobile:</span>9160478666</h5></div> 
                                                    </li>
    
                                                    <li>
                                                            <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                           <div class="info">
                                                               <h4>L.S.K.MEDICAL AGENCIES </h4>
                                                            <span>Ongole   </span>
                                                            <h3><span class="lbl">Person:</span>-</h3>
                                                            <h5><span class="lbl">Mobile:</span>-</h5></div> 
                                                </ul> 
                                    </div>
                                </div>
                            </div>


                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="heading7">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse7" aria-expanded="false" aria-controls="collapse7">
                                          
                                        GUNTUR
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading7">
                                    <div class="panel-body">
                                            <ul class="list-of">
                                                
                                            <li>
                                                            <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                           <div class="info">
                                                               <h4>SAHITHI MEDICAL AGENCIES</h4>
                                                            <span>Kothapet, Guntur</span>
                                                            <h3><span class="lbl">Person:</span>Ch.Raghavendra</h3>
                                                            <h5><span class="lbl">Mobile:</span>9963548810</h5></div> 
                                                        </li>
                                                    <li>
                                                        <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                       <div class="info">
                                                           <h4>DURGA HANUMAN MEDICAL AGENCIES</h4>
                                                        <span>Guntur</span>
                                                        <h3><span class="lbl">Person:</span>-</h3>
                                                        <h5><span class="lbl">Mobile:</span>-</h5></div> 
                                                    </li>
    
                                                    <li>
                                                            <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                           <div class="info">
                                                               <h4>SRINIVASA MEDICAL CORPORATION</h4>
                                                            <span>Guntur   </span>
                                                            <h3><span class="lbl">Person:</span>-</h3>
                                                            <h5><span class="lbl">Mobile:</span>-</h5></div> 
                                                        </li>
                                                </ul> 
                                    </div>
                                </div>
                            </div>


                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="heading8">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse8" aria-expanded="false" aria-controls="collapse8">
                                          
                                        VIJAYAWADA
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse8" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading8">
                                    <div class="panel-body">
                                            <ul class="list-of">
                                                
                                            <li>
                                                            <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                           <div class="info">
                                                               <h4>SREE SAIRAM PHARMACEUTICAL DISTRIBUTORS</h4>
                                                            <span>Behind DV Manor Hotel, Vijayawada</span>
                                                            <h3><span class="lbl">Person:</span>Kavitha</h3>
                                                            <h5><span class="lbl">Mobile:</span>0866-6622950</h5></div> 
                                                        </li>
                                                    <li>
                                                        <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                       <div class="info">
                                                           <h4>HANNS PHARMA </h4>
                                                        <span>Suryaraopet, Vijayawada</span>
                                                        <h3><span class="lbl">Person:</span>Radha Krishna</h3>
                                                        <h5><span class="lbl">Mobile:</span>9533470707, 0866-2436779</h5></div> 
                                                    </li>
    

                                                </ul> 
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="heading9">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse9" aria-expanded="false" aria-controls="collapse9">
                                          
                                        VIZAG
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse9" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading9">
                                    <div class="panel-body">
                                            <ul class="list-of">
                                                
                                            <li>
                                                            <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                           <div class="info">
                                                               <h4>BALAJI MEDICAL AGENCIES</h4>
                                                            <span>Dabagardens, Vizag</span>
                                                            <h3><span class="lbl">Person:</span>Navin</h3>
                                                            <h5><span class="lbl">Mobile:</span>8977727979</h5></div> 
                                                        </li>
                                                 </ul> 
                                    </div>
                                </div>
                            </div>


                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="heading10">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse10" aria-expanded="false" aria-controls="collapse10">
                                          
                                        RAJAHMUNDRY
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse10" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading10">
                                    <div class="panel-body">
                                            <ul class="list-of">
                                                
                                            <li>
                                                            <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                           <div class="info">
                                                               <h4>LAHARI MEDICAL AGENCIES</h4>
                                                            <span>Pappula Street, Rajahmundry</span>
                                                            <h3><span class="lbl">Person:</span>Y.Satyananda Rao</h3>
                                                            <h5><span class="lbl">Mobile:</span>8096463888 </h5></div> 
                                                        </li>


                                                        <li>
                                                            <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                           <div class="info">
                                                               <h4>VASAVI MEDICAL ENTERPRISES</h4>
                                                            <span>Pappula Street, Rajahmundry</span>
                                                            <h3><span class="lbl">Person:</span>Srinivasa Rao</h3>
                                                            <h5><span class="lbl">Mobile:</span>7036570465, 0883-2476130 </h5></div> 
                                                        </li>
                                                 </ul> 
                                    </div>
                                </div>
                            </div>


                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="heading11">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse11" aria-expanded="false" aria-controls="collapse11">
                                          
                                        KAKINADA
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse11" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading11">
                                    <div class="panel-body">
                                            <ul class="list-of">
                                                
                                            <li>
                                                            <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                           <div class="info">
                                                               <h4>SAI PRASANNA MEDICAL AGENCIES</h4>
                                                            <span>Bhudavarapu Street, Kakinada</span>
                                                            <h3><span class="lbl">Person:</span>Satyanarayana Murthy</h3>
                                                            <h5><span class="lbl">Mobile:</span>9290596146, 0884-6693363 </h5></div> 
                                                        </li>

                                                 </ul> 
                                    </div>
                                </div>
                            </div>


                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="heading12">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse12" aria-expanded="false" aria-controls="collapse12">
                                          
                                        AMALAPURAM
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse12" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading12">
                                    <div class="panel-body">
                                            <ul class="list-of">
                                                
                                            <li>
                                                            <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                           <div class="info">
                                                               <h4>SRI SAINATH MEDICAL AGENCIES</h4>
                                                            <span>Yerramalli Street, Amalapuram</span>
                                                            <h3><span class="lbl">Person:</span>M.V.Subba Rao</h3>
                                                            <h5><span class="lbl">Mobile:</span>9959381111, 08856-235602</h5></div> 
                                                        </li>

                                                 </ul> 
                                    </div>
                                </div>
                            </div>



                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="heading13">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse13" aria-expanded="false" aria-controls="collapse13">
                                          
                                        ELURU
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse13" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading13">
                                    <div class="panel-body">
                                            <ul class="list-of">
                                                
                                            <li>
                                                            <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                           <div class="info">
                                                               <h4>SAMBASIVA MEDICALS</h4>
                                                            <span>Eluru</span>
                                                            <h3><span class="lbl">Person:</span>-</h3>
                                                            <h5><span class="lbl">Mobile:</span>-</h5></div> 
                                                        </li>

                                                        <li>
                                                            <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                           <div class="info">
                                                               <h4>SAI GANESH MEDICAL MEDICAL  AGENCIES</h4>
                                                            <span>Chandana Brothers Road, Eluru</span>
                                                            <h3><span class="lbl">Person:</span>Kishore</h3>
                                                            <h5><span class="lbl">Mobile:</span>-</h5></div> 
                                                        </li>

                                                 </ul> 
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="heading14">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse14" aria-expanded="false" aria-controls="collapse14">
                                          
                                        TANUKU
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse14" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading14">
                                    <div class="panel-body">
                                            <ul class="list-of">
                                                
                                            <li>
                                                            <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                           <div class="info">
                                                               <h4>SIVA PARVATHI MEDICAL AND SURGICAL AGENCIES</h4>
                                                            <span>Ramineedu vari Street, Tanuku</span>
                                                            <h3><span class="lbl">Person:</span>Sateesh</h3>
                                                            <h5><span class="lbl">Mobile:</span>8466856566</h5></div> 
                                                        </li>
                                                 </ul> 
                                    </div>
                                </div>
                            </div>


                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="heading15">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse15" aria-expanded="false" aria-controls="collapse15">
                                          
                                        BHIMAVARAM
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse15" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading15">
                                    <div class="panel-body">
                                            <ul class="list-of">
                                                
                                            <li>
                                                            <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                           <div class="info">
                                                               <h4>SRINIDHI PHARMACEUTICALS</h4>
                                                            <span>Rest House Road, Bhimavaram</span>
                                                            <h3><span class="lbl">Person:</span>Vinod</h3>
                                                            <h5><span class="lbl">Mobile:</span>900019997,9948275729, 08816-235375</h5></div> 
                                                        </li>
                                                 </ul> 
                                    </div>
                                </div>
                            </div>


                            </div><!-- panel-group -->

                    </div>
                    <div role="tabpanel" class="tab-pane active" id="telangana">

                            <div class="panel-group" id="accordion2" role="tablist" aria-multiselectable="true">

                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="headingt1">
                                            <h4 class="panel-title">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapset1" aria-expanded="true" aria-controls="collapset1">
                                                    HYDERABAD
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapset1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingt1">
                                            <div class="panel-body">
                                                    <ul class="list-of">
                                                        <li>
                                                            <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                           <div class="info">
                                                               <h4>LENIMEN DISTRIBUTORS </h4>
                                                            <span>Kukutpally, Hyderabad</span>
                                                            <h3><span class="lbl">Person:</span>Prasad & Ravindra</h3>
                                                            <h5><span class="lbl">Mobile:</span>9100555060</h5></div> 
                                                        </li>
        
                                                        <li>
                                                                <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                               <div class="info">
                                                                   <h4>SRI NIRMALA PHARMA</h4>
                                                                <span>Secunderabad</span>
                                                                <h3><span class="lbl">Person:</span>Jagadish</h3>
                                                                <h5><span class="lbl">Mobile:</span>9246525992</h5></div> 
                                                            </li>
        
                                                            <li>
                                                                    <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                                   <div class="info">
                                                                       <h4>ALANKRITHA AGENCIES</h4>
                                                                    <span>Koti, Hyderabad</span>
                                                                    <h3><span class="lbl">Person:</span>Anand</h3>
                                                                    <h5><span class="lbl">Mobile:</span>9246150457</h5></div> 
                                                                </li>
                                                                <li>
                                                                        <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                                       <div class="info">
                                                                           <h4>VISHAL PHARMA</h4>
                                                                        <span>Koti, Hyderabad</span>
                                                                        <h3><span class="lbl">Person:</span>Vishal</h3>
                                                                        <h5><span class="lbl">Mobile:</span>9391117751</h5></div> 
                                                                    </li>
                                                                    <li>
                                                                            <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                                           <div class="info">
                                                                               <h4>OM SREE MEDISURGE</h4>
                                                                            <span>Kukutpally, Hyderabad</span>
                                                                            <h3><span class="lbl">Person:</span>Shiva</h3>
                                                                            <h5><span class="lbl">Mobile:</span>9100961967</h5></div> 
                                                                        </li>

                                                                    <li>
                                                                            <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                                           <div class="info">
                                                                               <h4>MARUTHI AGENCIES</h4>
                                                                            <span>MARUTHI AGENCIES </span>
                                                                            <h3><span class="lbl">Person:</span>Rathnesh</h3>
                                                                            <h5><span class="lbl">Mobile:</span>040-66660054</h5></div> 
                                                                        </li>
                                                                        
                                                                        <li>
                                                                            <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                                           <div class="info">
                                                                               <h4>RAMA SAI AGENCIES</h4>
                                                                            <span>Kukutpally, Hyderabad</span>
                                                                            <h3><span class="lbl">Person:</span>Ramasai</h3>
                                                                            <h5><span class="lbl">Mobile:</span>7207056813</h5></div> 
                                                                        </li>
                                                                        <li>
                                                                            <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                                           <div class="info">
                                                                               <h4>LAXMI MEDICAL AND SURGICAL AGENCIES</h4>
                                                                            <span>Bhongiri, Hyderabad</span>
                                                                            <h3><span class="lbl">Person:</span>Babu</h3>
                                                                            <h5><span class="lbl">Mobile:</span>9885162231, 9886744970</h5></div> 
                                                                        </li>
                                                                        <li>
                                                                            <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                                           <div class="info">
                                                                               <h4>MAHAN ACE PHARMA</h4>
                                                                            <span>Punjagutta, Hyderabad</span>
                                                                            <h3><span class="lbl">Person:</span>Ambarish</h3>
                                                                            <h5><span class="lbl">Mobile:</span>9396672277</h5></div> 
                                                                        </li>          
                                                                        <li>
                                                                            <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                                           <div class="info">
                                                                               <h4>SRI RANGA SAI AGENCIES</h4>
                                                                            <span>Punjagutta, Hyderabad</span>
                                                                            <h3><span class="lbl">Person:</span>Pavan Kumar</h3>
                                                                            <h5><span class="lbl">Mobile:</span>9700662555</h5></div> 
                                                                        </li>                                                                                                                                                                                                                                                                                                                                                               
                                                                        <li>
                                                                            <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                                           <div class="info">
                                                                               <h4>V.V.DISTRIBUTORS</h4>
                                                                            <span>Golnaka, Hyderabad</span>
                                                                            <h3><span class="lbl">Person:</span>Vishal</h3>
                                                                            <h5><span class="lbl">Mobile:</span>9908224262</h5></div> 
                                                                        </li> 
                                                                        <li>
                                                                            <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                                           <div class="info">
                                                                               <h4>NIRMALA AGENCIES</h4>
                                                                            <span>Indrabagh, Hyderabad</span>
                                                                            <h3><span class="lbl">Person:</span>Srikanth</h3>
                                                                            <h5><span class="lbl">Mobile:</span>9246824111</h5></div> 
                                                                        </li> 
                                                                        <li>
                                                                            <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                                           <div class="info">
                                                                               <h4>VASU AGENCIES</h4>
                                                                            <span>Himayat Nagar, Hyderabad</span>
                                                                            <h3><span class="lbl">Person:</span>Aravind</h3>
                                                                            <h5><span class="lbl">Mobile:</span>9849034156</h5></div> 
                                                                        </li>     
                                                                        <li>
                                                                            <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                                           <div class="info">
                                                                               <h4>SRI VENKATARAMANA AGENCIES </h4>
                                                                            <span>Dr.A.S.Rao Nagar, ECIL, Hyderabad</span>
                                                                            <h3><span class="lbl">Person:</span>Manohar</h3>
                                                                            <h5><span class="lbl">Mobile:</span>9100922788, 9391107955</h5></div> 
                                                                        </li> 
                                                                        <li>
                                                                            <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                                           <div class="info">
                                                                               <h4>ROHAN PHARMA</h4>
                                                                            <span>Kukutpally, Hyderabad</span>
                                                                            </div>
                                                                        </li> 
                                                    </ul> 
                                            </div>
                                        </div>
                                    </div>
                            
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="headingTwo2">
                                            <h4 class="panel-title">
                                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo2" aria-expanded="false" aria-controls="collapseTwo2">
                                                   
                                                    NIZAMABAD
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseTwo2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo2">
                                            <div class="panel-body">
                                                    <ul class="list-of">
                                                            <li>
                                                                <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                               <div class="info">
                                                                   <h4>Mahabubnagar</h4>
                                                                <span>Nizambad</span>
                                                                <h3><span class="lbl">Person:</span>Dharmaraj</h3>
                                                                <h5><span class="lbl">Mobile:</span>9490026365</h5></div> 
                                                            </li>
            
                                                        </ul> 
                                            </div>
                                        </div>
                                    </div>


                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="headingt3">
                                            <h4 class="panel-title">
                                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapset3" aria-expanded="false" aria-controls="collapset3">
                                                   
                                                    KAMAREDDY
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapset3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingt3">
                                            <div class="panel-body">
                                                    <ul class="list-of">
                                                            <li>
                                                                <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                               <div class="info">
                                                                   <h4>ABHIRAM MEDICAL AGENCIES</h4>
                                                                <span>Kamareddy</span>
                                                                <h3><span class="lbl">Person:</span>Narshimulu</h3>
                                                                <h5><span class="lbl">Mobile:</span>9299996602</h5></div> 
                                                            </li>
                                                            <li>
                                                                <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                               <div class="info">
                                                                   <h4>KAMAKSHI DRUGS</h4>
                                                                <span>Kamareddy</span>
                                                                <h3><span class="lbl">Person:</span>Nagaraju</h3>
                                                                <h5><span class="lbl">Mobile:</span>9989215833</h5></div> 
                                                            </li>
                                                        </ul> 
                                            </div>
                                        </div>
                                    </div>


                                     <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="headingt4">
                                            <h4 class="panel-title">
                                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapset4" aria-expanded="false" aria-controls="collapset4">
                                                   
                                                MAHABUBNAGAR
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapset4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingt4">
                                            <div class="panel-body">
                                                    <ul class="list-of">
                                                            <li>
                                                                <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                               <div class="info">
                                                                   <h4>SREE SRI HARI ENTERPRISES</h4>
                                                                <span>Mahabubnagar</span>
                                                                <h3><span class="lbl">Person:</span>Nagaraju</h3>
                                                                <h5><span class="lbl">Mobile:</span>9440743765</h5></div> 
                                                            </li>
                                                            <li>
                                                                <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                               <div class="info">
                                                                   <h4>SREE VENKATESWARA MEDICAL AND SURGICAL</h4>
                                                                <span>Mahabubnagar</span>
                                                                <h3><span class="lbl">Person:</span>SUBRAHMANYAM</h3>
                                                                <h5><span class="lbl">Mobile:</span>9032787308</h5></div> 
                                                            </li>
                                                            <li>
                                                                <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                               <div class="info">
                                                                   <h4>BALAJI MEDICAL AGENCIES</h4>
                                                                    <span>Mahabubnagar</span>
                                                                </div> 
                                                            </li>
                                                        </ul> 
                                            </div>
                                        </div>
                                    </div>


                                      <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="headingt5">
                                            <h4 class="panel-title">
                                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapset5" aria-expanded="false" aria-controls="collapset5">
                                                   
                                                NALGONDA
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapset5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingt5">
                                            <div class="panel-body">
                                                    <ul class="list-of">
                                                            <li>
                                                                <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                               <div class="info">
                                                                   <h4>SRI SRINIVASA AGENCIES</h4>
                                                                <span>Bottuguda</span>
                                                                <h3><span class="lbl">Person:</span>Venkateswarlu</h3>
                                                                <h5><span class="lbl">Mobile:</span>9849487999</h5></div> 
                                                            </li>
                                                            <li>
                                                                <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                               <div class="info">
                                                                   <h4>SRI LAKSHMI GANAPATHI MEDICAL AGENCIES</h4>
                                                                <span>Prakasham Bazar, Nalgonda</span>
                                                                <h3><span class="lbl">Person:</span>Srikanth</h3>
                                                                <h5><span class="lbl">Mobile:</span>9966307857</h5></div> 
                                                            </li>
                                                            <li>
                                                                <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                               <div class="info">
                                                                   <h4>SHIVA KAMAKSHI MEDICAL DISTRIBUTORS</h4>
                                                                    <span>Nalgonda</span>
                                                                </div> 
                                                            </li>
                                                        </ul> 
                                            </div>
                                        </div>
                                    </div>

                                     <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="headingt6">
                                            <h4 class="panel-title">
                                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapset6" aria-expanded="false" aria-controls="collapset6">
                                                   
                                                KHAMMAM
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapset6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingt6">
                                            <div class="panel-body">
                                                    <ul class="list-of">
                                                            <li>
                                                                <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                               <div class="info">
                                                                   <h4>ALMAS MEDICAL AGENCIES</h4>
                                                                <span>Khammam</span>
                                                                <h3><span class="lbl">Person:</span>Krishna Reddy</h3>
                                                                <h5><span class="lbl">Mobile:</span>9440419778</h5></div> 
                                                            </li>
                                                            <li>
                                                                <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                               <div class="info">
                                                                   <h4>NAGARJUNA MEDICAL DISTRIBUTORS</h4>
                                                                <span>NST Road, Khammam</span>
                                                                <h3><span class="lbl">Person:</span>Srinivasa Rao</h3>
                                                                <h5><span class="lbl">Mobile:</span>9963506235</h5></div> 
                                                            </li>
                                                            <li>
                                                                <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                               <div class="info">
                                                                   <h4>NANDAGOPAL MEDICAL AGENCIES </h4>
                                                                    <span>Khammam</span>
                                                                </div> 
                                                            </li>
                                                        </ul> 
                                            </div>
                                        </div>
                                    </div>

                                     <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="headingt7">
                                            <h4 class="panel-title">
                                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapset7" aria-expanded="false" aria-controls="collapset7">
                                                   
                                                WARANGAL
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapset7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingt7">
                                            <div class="panel-body">
                                                    <ul class="list-of">
                                                            <li>
                                                                <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                               <div class="info">
                                                                   <h4>SREE MAHALAKSHMI AGENCIES</h4>
                                                                <span>Hanamkoda, Warangal</span>
                                                                <h3><span class="lbl">Person:</span>Naresh</h3>
                                                                <h5><span class="lbl">Mobile:</span>9391559377</h5></div> 
                                                            </li>
                                                            <li>
                                                                <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                               <div class="info">
                                                                   <h4>SRI SRI AGENCIES </h4>
                                                                <span>Warangal</span>
                                                                </div> 
                                                            </li>
                                                            <li>
                                                                <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                               <div class="info">
                                                                   <h4>LAKSHMI SAI MEDICAL AGENCIES </h4>
                                                                    <span>Warangal</span>
                                                                    <h3><span class="lbl">Person:</span>Srikanth</h3>
                                                                     <h5><span class="lbl">Mobile:</span>9246752402</h5>
                                                                
                                                                </div> 
                                                            </li>
                                                            <li>
                                                                <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                               <div class="info">
                                                                   <h4>KUMARASWAMY MEDICAL AGENCIES</h4>
                                                                    <span>Warangal</span>
                                                                      
                                                                </div> 
                                                            </li>   
                                                            <li>
                                                                <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                               <div class="info">
                                                                   <h4>PRASANNA PHARMA</h4>
                                                                    <span>Warangal</span>
                                                                    <h3><span class="lbl">Person:</span>Amarnath</h3>
                                                                     <h5><span class="lbl">Mobile:</span>9989737573</h5>
                                                                
                                                                </div> 
                                                            </li>                                                                                                                      
                                                        </ul> 
                                            </div>
                                        </div>
                                    </div>
                            
                                   
                            
                                </div><!-- panel-group -->


                    </div>

                    
                    <div role="tabpanel" class="tab-pane" id="karnataka">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

                        <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingk1">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsek1" aria-expanded="true" aria-controls="collapsek1">
                                        DAVANGERE
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapsek1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingk1">
                                    <div class="panel-body">
                                            <ul class="list-of">
                                                <li>
                                                    <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                   <div class="info">
                                                       <h4>AKP HEALTHCARE PRIVATE LIMITED</h4>
                                                     <span>Hadadi Road, Opp: Taralabalu School</span>
                                                 </div> 
                                                </li>
                                            </ul> 
                                    </div>
                                </div>
                            </div>



<div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingk2">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsek2" aria-expanded="false" aria-controls="collapsek2" class="collapsed">
                                        BELLARY
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapsek2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingk2">
                                    <div class="panel-body">
                                            <ul class="list-of">
                                                <li>
                                                    <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                   <div class="info">
                                                       <h4>BHARATHI MEDICAL AGENCIES</h4>
                                                     <span>NEAR GOPI BLOOD BANK,PARAVATHI NAGAR</span>
                                                 </div> 
                                                </li>
                                            </ul> 
                                    </div>
                                </div>
                            </div>




                              <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingk3">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsek3" aria-expanded="false" aria-controls="collapsek3" class="collapsed">
                                        GULBARGA
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapsek3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingk3">
                                    <div class="panel-body">
                                            <ul class="list-of">
                                                <li>
                                                    <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                   <div class="info">
                                                       <h4>GURUDATTA AGENCIES</h4>
                                                     <span>KING KOURT COMLEX,GULBARGA</span>
                                                     <h5><span class="lbl">Mobile:</span>9342331159</h5>

                                                 </div> 
                                                </li>
                                            </ul> 
                                    </div>
                                </div>
                            </div>


                             <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingk4">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsek4" aria-expanded="false" aria-controls="collapsek4" class="collapsed">
                                        SHIMOGA
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapsek4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingk4">
                                    <div class="panel-body">
                                            <ul class="list-of">
                                                <li>
                                                    <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                   <div class="info">
                                                       <h4>GURUDATTA AGENCIES</h4>
                                                     <span>M.K.K ROAD, SHIMOGA</span>
                                                     <h3><span class="lbl">Person:</span>SRI RAJENDRA</h3>                                                     
                                                     <h5><span class="lbl">Mobile:</span>08182-224152,260342</h5>

                                                 </div> 
                                                </li>
                                            </ul> 
                                    </div>
                                </div>
                            </div>



                     <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingk5">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsek5" aria-expanded="false" aria-controls="collapsek5" class="collapsed">
                                        HUBLI
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapsek5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingk5">
                                    <div class="panel-body">
                                            <ul class="list-of">
                                                <li>
                                                    <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                   <div class="info">
                                                       <h4>YASHIKA  AGENCIES</h4>
                                                     <span>SHOP NO:201, SHIMOGA</span>
                                                     <h5><span class="lbl">Mobile:</span>9060979491, 2365441,2221305</h5>

                                                 </div> 
                                                </li>
                                            </ul> 
                                    </div>
                                </div>
                                
                            </div>



                            
                      <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingk6">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsek6" aria-expanded="false" aria-controls="collapsek6" class="collapsed">
                                        BELGAUM
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapsek6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingk6">
                                    <div class="panel-body">
                                            <ul class="list-of">
                                                <li>
                                                    <i class="icon"><img alt="" src="<?php echo $CONFIG_SERVER_ROOT;?>images/loction-new-in.png"></i>
                                                   <div class="info">
                                                       <h4>ROYAL MEDICAL AGENCIES</h4>
                                                     <span>Aravind Complex, Maruti Galli, Belgaum</span>
                                                 </div> 
                                                </li>
                                            </ul> 
                                    </div>
                                </div>
                            </div>


                   
                   
                    </div>
                    
                </div>


            </div>
        </div>
      
            

    </div>
  </section>
  
  <section class="action-band">
      <div class="container">
           <div class="row call-to-action fadeInUp wow">
               <div class="col-md-9">
                   <p>We are proud to have the opportunity to 
                       give you the Health of your dreams.</p>
               </div>
               <div class="col-md-3">
                   <a href="#" class="bnt btn-primary cust-btn-outline">Contact Us Now !</a>
               </div>
           </div>
      </div>
   
    </section>
    <?php include('footer.php'); ?>