<?php include('header.php'); ?>
<section class="inner-banner-section fadeIn wow">
    <div class="solid-bg">
      <div class="container">
        <div class="banner-title fadeInUp wow" data-wow-delay=".2s">
          <h1>We Care About Your Life</h1>
          <p>To inspire hope and contribute to health and well-being by providing 
              the best care to every patient.</p>
          <div class="banner-breadcrumbs"> <span><a href="index.html" class="home"><span>Home</span></a></span> <span class="sep">/</span> <span class="current">Products</span> </div>
        </div>
      </div>
    </div>
  </section>
<section class="services-block gray-bg-block">
    <div class="container">
        <div class="row">
            <div class="col-md-12 portfolio-nav">
                <ul>
                       <!-- For filtering controls add -->
                       <li data-filter="all" class="active"> 
                        <span class="icon"> <img src="images/all-icon.png"></span>
                        <h4>All</h4> 
                      </li>
                       <li data-filter="1"> <span class="icon"><img src="images/neuro-icon.png"></span> <h4>NEURO</h4> </li>
                       <li data-filter="2"><span class="icon"><img src="images/ortho-icon.png"></span><h4>ORTHO</h4> </li>
                       <li data-filter="3"><span class="icon"><img src="images/psy-icon.png"></span><h4> PSYCHIATRY</h4> </li>
                    <li data-filter="4"><span class="icon"><img src="images/opt-icon.png"></span> <h4>OPTHALMOLOGY</h4> </li>
                    <li data-filter="5"> <span class="icon"><img src="images/gyn-icon.png"></span><h4>GYNAECOLOGY</h4> </li>
                    <li data-filter="6"> <span class="icon"><img src="images/gen-icon.png"></span> <h4>General</h4> </li>
                      
                    </ul>
            </div>
        </div>
      <div class="row">
       

            <div class="filtr-container">
                <div class="col-xs-12 col-sm-4 col-md-4 filtr-item" data-category="1" data-sort="">
                  
                  <div class="filtr-item-block">
                     <img src="images/citibrisk2ml.png" class="img-responsive" alt="CITIBRISK-2ML"/>
                      <ul>
                        <li class="line-clamp"><span>MOLECULE:</span>CITICOLINE  500 MG  (2ML INJ)</li>
                        <li><span>FORM:</span> INJECTION </li>
                        <li><span>THERAPY AREAS:</span> NEURO  PROTECTOR AND COGNITIVE ENHANCER</li>
                      </ul>
                     
                  </div>
                </div>


                <div class="col-xs-12 col-sm-4 col-md-4 filtr-item" data-category="1" data-sort="">
                  
                  <div class="filtr-item-block">
                     <img src="images/citibrisk500.png" class="img-responsive" alt="CITIBRISK-500 MG"/>
                      <ul>
                        <li class="line-clamp"><span>MOLECULE:</span>CITICOLINE 500 MG TAB</li>
                        <li><span>FORM:</span> TABLET </li>
                        <li><span>THERAPY AREAS:</span> NEURO  PROTECTOR AND COGNITIVE ENHANCER</li>
                      </ul>
                     
                  </div>
                </div>

                <div class="col-xs-12 col-sm-4 col-md-4 filtr-item" data-category="1" data-sort="">
                  
                  <div class="filtr-item-block">
                     <img src="images/citibrisk+.png" class="img-responsive" alt="CITIBRISK-PLUS"/>
                      <ul>
                        <li class="line-clamp"><span>MOLECULE:</span>CITICOLINE&PIRACETAM  800 MG TABLETS</li>
                        <li><span>FORM:</span> TABLET </li>
                        <li><span>THERAPY AREAS:</span>NUERO PROTECTOR & COGNITIVE ENHANCER</li>
                      </ul>
                     
                  </div>
                </div>


                <div class="col-xs-12 col-sm-4 col-md-4 filtr-item" data-category="1" data-sort="">
                  
                  <div class="filtr-item-block">
                     <img src="images/gabaprimem.png" class="img-responsive" alt="GABAPRIME-M TAB"/>
                      <ul>
                        <li class="line-clamp"><span>MOLECULE:</span>GABAPENTIN 300 MG+METHYCOBALAMIN 500 MCG TAB</li>
                        <li><span>FORM:</span> TABLET </li>
                        <li><span>THERAPY AREAS:</span>GABAPENTIN 300 MG+METHYCOBALAMIN 500 MCG TAB</li>
                      </ul>
                     
                  </div>
                </div>

                

                <div class="col-xs-12 col-sm-4 col-md-4 filtr-item" data-category="1" data-sort="">
                  
                  <div class="filtr-item-block">
                     <img src="images/gabant.png" class="img-responsive" alt="GABAPRIME-NT"/>
                      <ul>
                        <li class="line-clamp"><span>MOLECULE:</span>GABAPENTIN 400 MG+NORTRIPTYLINE10MG TAB</li>
                        <li><span>FORM:</span> TABLET </li>
                        <li><span>THERAPY AREAS:</span>GABAPENTIN 400 MG+NORTRIPTYLINE10MG TAB</li>
                      </ul>
                     
                  </div>
                </div>

                
                <div class="col-xs-12 col-sm-4 col-md-4 filtr-item" data-category="1" data-sort="">
                  
                  <div class="filtr-item-block">
                     <img src="images/gabatrio.png" class="img-responsive" alt="GABAPRIME-TRIO"/>
                      <ul>
                        <li class="line-clamp" data-toggle="popover"  data-placement="top" title="MOLECULE" data-content="GABAPENTIN 100 MG+NORTRIPTYLINE 10MG+MECOBALAMIN 750 MCG"><span>MOLECULE:</span>GABAPENTIN 100 MG+NORTRIPTYLINE 10MG+MECOBALAMIN 750 MCG</li>
                        <li><span>FORM:</span> TABLET </li>
                        <li><span>THERAPY AREAS:</span>ANTI CONVULSANT,ANTI DEPRESSANT AND VITAMIN B12</li>
                      </ul>
                     
                  </div>
                </div>


                <div class="col-xs-12 col-sm-4 col-md-4 filtr-item" data-category="1" data-sort="">
                  
                  <div class="filtr-item-block">
                     <img src="images/neuro25.png" class="img-responsive" alt="NEUROSTAB 25"/>
                      <ul>
                        <li class="line-clamp"><span>MOLECULE:</span>PREGABALIN 25 MG TABLETS</li>
                        <li><span>FORM:</span> TABLET </li>
                        <li><span>THERAPY AREAS:</span>ANTI CONVULSANT</li>
                      </ul>
                     
                  </div>
                </div>


                <div class="col-xs-12 col-sm-4 col-md-4 filtr-item" data-category="1" data-sort="">
                  
                  <div class="filtr-item-block">
                     <img src="images/neuro75.png" class="img-responsive" alt="NEUROSTAB-75"/>
                      <ul>
                        <li class="line-clamp"><span>MOLECULE:</span>PREGABALIN 75 MG TABLETS</li>
                        <li><span>FORM:</span> TABLET </li>
                        <li><span>THERAPY AREAS:</span>ANTI CONVULSANT</li>
                      </ul>
                     
                  </div>
                </div>

                
                <div class="col-xs-12 col-sm-4 col-md-4 filtr-item" data-category="1" data-sort="">
                  
                  <div class="filtr-item-block">
                     <img src="images/neurostabm75.png" class="img-responsive" alt="NEUROSTAB-M75"/>
                      <ul>
                        <li class="line-clamp"><span>MOLECULE:</span>METHYLCOBALAMIN 750 MCG+PREGABALIN 75 MG</li>
                        <li><span>FORM:</span> TABLET </li>
                        <li><span>THERAPY AREAS:</span>ANTI CONVULSANT AND VITAMIN B12</li>
                      </ul>
                     
                  </div>
                </div>


                <div class="col-xs-12 col-sm-4 col-md-4 filtr-item" data-category="1" data-sort="">
                  
                  <div class="filtr-item-block">
                     <img src="images/neuront.png" class="img-responsive" alt="NEUROSTAB-NT"/>
                      <ul>
                        <li class="line-clamp"><span>MOLECULE:</span>NEUROSTAB-NT</li>
                        <li><span>FORM:</span> TABLET </li>
                        <li><span>THERAPY AREAS:</span>ANTI CONVULSANT AND ANTI DEPRESSANT</li>
                      </ul>
                     
                  </div>
                </div>


                <div class="col-xs-12 col-sm-4 col-md-4 filtr-item" data-category="1" data-sort="">
                  
                  <div class="filtr-item-block">
                     <img src="images/sleepmax.png" class="img-responsive" alt="SLEEP MAX"/>
                      <ul>
                        <li class="line-clamp" data-toggle="popover"  data-placement="top" title="MOLECULE" data-content="L-THEANINE 100 MG +MELATONIN 3MG+5TH (GRIFFONIA SIMPLICIFOLIA)30 MG TABLETS"><span>MOLECULE:</span>L-THEANINE 100 MG +MELATONIN 3MG+5TH (GRIFFONIA SIMPLICIFOLIA)30 MG TABLETS</li>
                        <li><span>FORM:</span> TABLET </li>
                        <li><span>THERAPY AREAS:</span>MEDICAL FOOD REGULATES THE BIOLOGICAL CLOCK</li>
                      </ul>
                     
                  </div>
                </div>


                <div class="col-xs-12 col-sm-4 col-md-4 filtr-item" data-category="1" data-sort="">
                  
                  <div class="filtr-item-block">
                     <img src="images/foltrendplus.png" class="img-responsive" alt="FOLTREND PLUS"/>
                      <ul>
                        <li class="line-clamp"><span>MOLECULE:</span>FOLTREND PLUS</li>
                        <li><span>FORM:</span> TABLET </li>
                        <li><span>THERAPY AREAS:</span>FOLTREND PLUS</li>
                      </ul>
                     
                  </div>
                </div>

                
                <div class="col-xs-12 col-sm-4 col-md-4 filtr-item" data-category="1" data-sort="">
                  
                  <div class="filtr-item-block">
                     <img src="images/theraspine.png" class="img-responsive" alt="THERASPINE"/>
                      <ul>
                        <li class="line-clamp" data-toggle="popover"  data-placement="top" title="MOLECULE" data-content="Choline Bitartrate 125mg + L-Arginine 75mg + L-Glutamic Acid 50mg + L-Histidine 50mg + Whey Protein Isolate 75mg + L-Serine 25mg + Griffonia Simplicifolia 32mg + Cocoa Extract 50mg + Grape Seed Extract 25mg + Ciinamon 25mg + Gamma Aminobutyric Acid 200mg" ><span>MOLECULE:</span>Choline Bitartrate 125mg + L-Arginine 75mg + L-Glutamic Acid 50mg + L-Histidine 50mg + Whey Protein Isolate 75mg + L-Serine 25mg + Griffonia Simplicifolia 32mg + Cocoa Extract 50mg + Grape Seed Extract 25mg + Ciinamon 25mg + Gamma Aminobutyric Acid 200mg</li>
                        <li><span>FORM:</span> TABLET </li>
                        <li><span>THERAPY AREAS:</span>BIOGENIC AMINES TO TREAT LOW BACK PAIN</li>
                      </ul>
                     
                  </div>
                </div>


                                
                <div class="col-xs-12 col-sm-4 col-md-4 filtr-item" data-category="1" data-sort="">
                  
                  <div class="filtr-item-block">
                     <img src="images/cognilong.png" class="img-responsive" alt="COGNILONG"/>
                      <ul>
                        <li class="line-clamp" data-toggle="popover"  data-placement="top" title="MOLECULE" data-content="Cerebroprotein Hydrolysate 90mg+Piracetam800mg+ Methylcobalamin500mcg+Ascorbic Acid90mg+Cholecalciferol 1000IU+Tocopheryl Acetate50IU+Thiamine Mononitrate10mg+Folic Acid2.5mg tabs"><span>MOLECULE:</span>Cerebroprotein Hydrolysate 90mg+Piracetam800mg+ Methylcobalamin500mcg+Ascorbic Acid90mg+Cholecalciferol 1000IU+Tocopheryl Acetate50IU+Thiamine Mononitrate10mg+Folic Acid2.5mg tabs</li>
                        <li><span>FORM:</span> TABLET </li>
                        <li><span>THERAPY AREAS:</span>NEURO PROTECTOR IN THE MANAGEMENT OF STROKE COMPLICATIONS</li>
                      </ul>
                     
                  </div>
                </div>


                <div class="col-xs-12 col-sm-4 col-md-4 filtr-item" data-category="1" data-sort="">
                  
                  <div class="filtr-item-block">
                     <img src="images/gabaprimem.png" class="img-responsive" alt="GABAPRIME PLUS GEL"/>
                      <ul>
                        <li class="line-clamp"><span>MOLECULE:</span>GABAPRIME PLUS GEL</li>
                        <li><span>FORM:</span> GEL </li>
                        <li><span>THERAPY AREAS:</span>TOPICAL APPLICATION FOR WIDE VARIETIES OF PAINFUL CONDITIONS</li>
                      </ul>
                     
                  </div>
                </div>



                
                

                  <div class="col-xs-12 col-sm-4 col-md-4 filtr-item" data-category="2" data-sort="">
                      <div class="filtr-item-block">
                          <img src="images/zacdef.png" class="img-responsive" alt="ZACDEF-6"/>
                           <ul>
                             <li class="line-clamp"><span>MOLECULE:</span>DEFLAZACORT 6 MG TAB</li>
                             <li><span>FORM:</span> TABLET</li>
                             <li><span>THERAPY AREAS:</span>GLUCOCORTICOID OF CHOICE IN CHRONIC INFLAMMATORY CONDITIONS</li>

                           </ul>
                          
                       </div>
                         


                    </div>



                    
                  <div class="col-xs-12 col-sm-4 col-md-4 filtr-item" data-category="2" data-sort="">
                      <div class="filtr-item-block">
                          <img src="images/reltracet.png" class="img-responsive" alt="RELTRACET TAB"/>
                           <ul>
                             <li class="line-clamp"><span>MOLECULE:</span>TRAMADOL HCL & PARACETAMOL TABLETS</li>
                             <li><span>FORM:</span> TABLET</li>
                             <li><span>THERAPY AREAS:</span>ACUTE AND CHRONIC PAINFUL CONDITIONS</li>

                           </ul>
                          
                       </div>
                         


                    </div>


                    <div class="col-xs-12 col-sm-4 col-md-4 filtr-item" data-category="2" data-sort="">
                      <div class="filtr-item-block">
                          <img src="images/6art.png" class="img-responsive" alt="6-ART"/>
                           <ul>
                             <li class="line-clamp" data-toggle="popover"  data-placement="top" title="MOLECULE" data-content="BOSWELLIC ACID,GLUCOSAMINE SULPHATE,MSM,CHONDROITIN SUPHATE,CURCUMIN AND HYALURONIC ACID TABLETS"><span>MOLECULE:</span>BOSWELLIC ACID,GLUCOSAMINE SULPHATE,MSM,CHONDROITIN SUPHATE,CURCUMIN AND HYALURONIC ACID TABLETS</li>
                             <li><span>FORM:</span> TABLET</li>
                             <li><span>THERAPY AREAS:</span>MEDICAL  FOOD SUPPLEMENTATION FOR ARTHITIS</li>

                           </ul>
                          
                       </div>
                         


                    </div>

                    
                    <div class="col-xs-12 col-sm-4 col-md-4 filtr-item" data-category="2" data-sort="">
                      <div class="filtr-item-block">
                          <img src="images/zacdef.png" class="img-responsive" alt="ZACDEF-30"/>
                           <ul>
                             <li class="line-clamp"><span>MOLECULE:</span>DEFLAZACORT TABLETS</li>
                             <li><span>FORM:</span> TABLET</li>
                             <li><span>THERAPY AREAS:</span>DEFLAZACORT TABLETS</li>

                           </ul>
                          
                       </div>
                         


                    </div>
                    


                   
                    <div class="col-xs-12 col-sm-4 col-md-4 filtr-item" data-category="3" data-sort="">
                        <div class="filtr-item-block">
                            <img src="images/euluga.png" class="img-responsive" alt="ELUGA-5MG TAB"/>
                             <ul>
                               <li class="line-clamp"><span>MOLECULE:</span>ESCITALOPRAM OXALATE TABLETS I.P 5MG TAB</li>
                               <li><span>FORM:</span>TABLET</li>
                               <li><span>THERAPY AREAS:</span>ANTI  DEPRESSANT FOR GENERAL ANXIETY DISORDERS</li>

                             </ul>
                            
                         </div>
                      </div>

                      <div class="col-xs-12 col-sm-4 col-md-4 filtr-item" data-category="3" data-sort="">
                        <div class="filtr-item-block">
                            <img src="images/euluga.png" class="img-responsive" alt="ELUGA-10 MG TAB"/>
                             <ul>
                               <li class="line-clamp"><span>MOLECULE:</span> ESCITALOPRAM OXALATE TABLETS I.P 10MG TAB </li>
                               <li><span>FORM:</span> TABLET  </li>
                               <li><span>THERAPY AREAS:</span>ANTI  DEPRESSANT FOR GENERAL ANXIETY DISORDERS</li>

                             </ul>
                            
                         </div>
                      </div>

                      <div class="col-xs-12 col-sm-4 col-md-4 filtr-item" data-category="3" data-sort="">
                        <div class="filtr-item-block">
                            <img src="images/euluga.png" class="img-responsive" alt="ELUGA- PLUS"/>
                             <ul>
                             <li class="line-clamp"><span>MOLECULE:</span> ESCITOLOPRAM OXALATE 5 MG+CLONAZEPAM 0.25 MG TAB </li>
                             <li><span>FORM:</span> TABLET  </li>
                             <li><span>THERAPY AREAS:</span>ESCITOLOPRAM OXALATE 5 MG+CLONAZEPAM 0.25 MG TAB</li>

                             </ul>
                            
                         </div>
                      </div>

                      <div class="col-xs-12 col-sm-4 col-md-4 filtr-item" data-category="3" data-sort="">
                      <div class="filtr-item-block">
                          <img src="images/euluga.png" class="img-responsive" alt="ELUGA-FORTE"/>
                           <ul>
                           <li class="line-clamp"><span>MOLECULE:</span> ESCITOLOPRAM OXALATE 10 MG+CLONAZEPAM 0.25 MG TAB</li>
                           <li><span>FORM:</span> TABLET  </li>
                           <li><span>THERAPY AREAS:</span>ESCITOLOPRAM OXALATE 10 MG+CLONAZEPAM 0.25 MG TAB</li>

                           </ul>
                          
                       </div>
                    </div>


                    <div class="col-xs-12 col-sm-4 col-md-4 filtr-item" data-category="4" data-sort="">
                    <div class="filtr-item-block">
                        <img src="images/astazea.png" class="img-responsive" alt="ASTAZEA"/>
                         <ul>
                         <li class="line-clamp" data-toggle="popover"  data-placement="top" title="MOLECULE" data-content=" Astaxanthin 8mg+Zeaxathin 2mg+Lutein 10%(Vegetable Source) 10mg tablets"><span>MOLECULE:</span> Astaxanthin 8mg+Zeaxathin 2mg+Lutein 10%(Vegetable Source) 10mg tablets</li>
                         <li><span>FORM:</span> TABLET  </li>
                         <li><span>THERAPY AREAS:</span>ANTI OXIDANT</li>

                         </ul>
                        
                     </div>
                  </div>


                  <div class="col-xs-12 col-sm-4 col-md-4 filtr-item" data-category="4" data-sort="">
                    <div class="filtr-item-block">
                        <img src="images/aquagold.png" class="img-responsive" alt="AQUGOLD 10ml Drops"/>
                         <ul>
                         <li class="line-clamp"><span>MOLECULE:</span> Carboxymethylcelulose Eye Drops I.P.1.0%W/V</li>
                         <li><span>FORM:</span> DROPS  </li>
                         <li><span>THERAPY AREAS:</span>OCULAR LUBRICANT FOR SOOTHING OF EYES</li>

                         </ul>
                        
                     </div>
                  </div>


                  <div class="col-xs-12 col-sm-4 col-md-4 filtr-item" data-category="5" data-sort="">
                    <div class="filtr-item-block">
                        <img src="images/letojet.png" class="img-responsive" alt="LETOJET Tablets"/>
                         <ul>
                         <li class="line-clamp"><span>MOLECULE:</span> Letrozole USP 2.5mg film coated tablet</li>
                         <li><span>FORM:</span> TABLET  </li>
                         <li><span>THERAPY AREAS:</span>ANTI ESTROGEN</li>

                         </ul>
                        
                     </div>
                  </div>

                  <div class="col-xs-12 col-sm-4 col-md-4 filtr-item" data-category="5" data-sort="">
                    <div class="filtr-item-block">
                        <img src="images/myotrend.png" class="img-responsive" alt="MYOTREND Softgel Capsules"/>
                         <ul>
                         <li class="line-clamp" data-toggle="popover"  data-placement="top" title="MOLECULE" data-content="Astaxanthin16mg+Inositol(Myo-Inositol)550mg+D-Chiro-Inositol 13.8mg+Berberine Hydrochloride 277.25mg+Chromium Picolinate200mcg+Folic Acid1.5mg+Vitamin D3 400IU+Lycopene 10% 5mg+Melatonin 3mg+DHA 75mg soft gelatin capsule"><span>MOLECULE:</span> Astaxanthin16mg+Inositol(Myo-Inositol)550mg+D-Chiro-Inositol 13.8mg+Berberine Hydrochloride 277.25mg+Chromium Picolinate200mcg+Folic Acid1.5mg+Vitamin D3 400IU+Lycopene 10% 5mg+Melatonin 3mg+DHA 75mg soft gelatin capsule</li>
                         <li><span>FORM:</span> SOFT GEL CAPSULE  </li>
                         <li class="line-clamp" data-toggle="popover"  data-placement="top" title="THERAPY AREAS" data-content="Astaxanthin16mg+Inositol(Myo-Inositol)550mg+D-Chiro-Inositol 13.8mg+Berberine Hydrochloride 277.25mg+Chromium Picolinate200mcg+Folic Acid1.5mg+Vitamin D3 400IU+Lycopene 10% 5mg+Melatonin 3mg+DHA 75mg soft gelatin capsule"><span>THERAPY AREAS:</span>Astaxanthin16mg+Inositol(Myo-Inositol)550mg+D-Chiro-Inositol 13.8mg+Berberine Hydrochloride 277.25mg+Chromium Picolinate200mcg+Folic Acid1.5mg+Vitamin D3 400IU+Lycopene 10% 5mg+Melatonin 3mg+DHA 75mg soft gelatin capsule</li>

                         </ul>
                        
                     </div>
                  </div>

                  <div class="col-xs-12 col-sm-4 col-md-4 filtr-item" data-category="5" data-sort="">
                    <div class="filtr-item-block">
                        <img src="images/utiheal.png" class="img-responsive" alt="UTIHEAL Capsules"/>
                         <ul>
                         <li class="line-clamp"><span>MOLECULE:</span> UTIHEAL Capsules</li>
                         <li><span>FORM:</span> CAPSULE  </li>
                         <li><span>THERAPY AREAS:</span> NON ANTIBIOTIC POWER IN RECURRENT URINARY TRACT INFECTIONS</li>

                         </ul>
                        
                     </div>
                  </div>


                  
                  <div class="col-xs-12 col-sm-4 col-md-4 filtr-item" data-category="5" data-sort="">
                    <div class="filtr-item-block">
                        <img src="images/protrend.png" class="img-responsive" alt="PROTREND POWDER"/>
                         <ul>
                         <li class="line-clamp"><span>MOLECULE:</span> PROTREND POWDER</li>
                         <li><span>FORM:</span> POWDER  </li>
                         <li><span>THERAPY AREAS:</span> PROTEIN POWDER IN COMPLETE NUTRITIONAL SUPPORT</li>

                         </ul>
                        
                     </div>
                  </div>


                                    
                  <div class="col-xs-12 col-sm-4 col-md-4 filtr-item" data-category="5" data-sort="">
                    <div class="filtr-item-block">
                        <img src="images/trendyferxt.png" class="img-responsive" alt="TRENDYFER  XT Tablets"/>
                         <ul>
                         <li class="line-clamp"><span>MOLECULE:</span> Ferrous Ascorbate Equivalent to elemental Iron 100mg+Folic Acid IP 1.5mg+Zinc Sulphate Monihydrate IP Equivalent to elemental Zinc 22.5mg film coated tablet</li>
                         <li><span>FORM:</span> POWDER  </li>
                         <li><span>THERAPY AREAS:</span> STANDARD ORAL IRON THERAPY</li>

                         </ul>
                        
                     </div>
                  </div>

                  <div class="col-xs-12 col-sm-4 col-md-4 filtr-item" data-category="6" data-sort="">
                    <div class="filtr-item-block">
                        <img src="images/calquickd3.png" class="img-responsive" alt="CALQUICK-D3"/>
                         <ul>
                         <li class="line-clamp"><span>MOLECULE:</span> CALQUICK-D3</li>
                         <li><span>FORM:</span> TABLET  </li>
                         <li><span>THERAPY AREAS:</span> CALCIUM SUPPLEMENTATION</li>

                         </ul>
                        
                     </div>
                  </div>

                  <div class="col-xs-12 col-sm-4 col-md-4 filtr-item" data-category="6" data-sort="">
                    <div class="filtr-item-block">
                        <img src="images/panqk.png" class="img-responsive" alt="PANQUICK-40 TAB"/>
                         <ul>
                         <li class="line-clamp"><span>MOLECULE:</span> PANTAPRAZOLE SODIUM 40MG TAB</li>
                         <li><span>FORM:</span> TABLET  </li>
                         <li><span>THERAPY AREAS:</span> PANTAPRAZOLE SODIUM 40MG TAB</li>

                         </ul>
                        
                     </div>
                  </div>

                  <div class="col-xs-12 col-sm-4 col-md-4 filtr-item" data-category="6" data-sort="">
                    <div class="filtr-item-block">
                        <img src="images/panqk.png" class="img-responsive" alt="PANQUICK-D TAB"/>
                         <ul>
                         <li class="line-clamp"><span>MOLECULE:</span> PANTAPRAZOLE SODIUM 40MG&DOMEPERIDONE 10 MG TAB</li>
                         <li><span>FORM:</span> TABLET  </li>
                         <li><span>THERAPY AREAS:</span> ACID CONTROL AND GASTRIC EMPTYING </li>

                         </ul>
                        
                     </div>
                  </div>


                  
                  <div class="col-xs-12 col-sm-4 col-md-4 filtr-item" data-category="6" data-sort="">
                    <div class="filtr-item-block">
                        <img src="images/panqk.png" class="img-responsive" alt="PANQUICK-DSR CAP"/>
                         <ul>
                         <li class="line-clamp"><span>MOLECULE:</span> PANQUICK-DSR CAP</li>
                         <li><span>FORM:</span> TABLET  </li>
                         <li><span>THERAPY AREAS:</span> ACID CONTROL AND GASTRIC EMPTYING </li>

                         </ul>
                        
                     </div>
                  </div>


                  <div class="col-xs-12 col-sm-4 col-md-4 filtr-item" data-category="6" data-sort="">
                    <div class="filtr-item-block">
                        <img src="images/primecobal.png" class="img-responsive" alt="PRIMECOBAL-500 INJ"/>
                         <ul>
                         <li class="line-clamp"><span>MOLECULE:</span> VITAMIN B12 IN THE TREATMENT OF NEUROPATHY AND DEFICIENCY</li>
                         <li><span>FORM:</span> INJECTION  </li>
                         <li><span>THERAPY AREAS:</span> VITAMIN B12 IN THE TREATMENT OF NEUROPATHY AND DEFICIENCY </li>

                         </ul>
                        
                     </div>
                  </div>

                  <div class="col-xs-12 col-sm-4 col-md-4 filtr-item" data-category="6" data-sort="">
                    <div class="filtr-item-block">
                        <img src="images/primecobal_1.png" class="img-responsive" alt="PRIMECOBAL-1000 INJ"/>
                         <ul>
                         <li class="line-clamp"><span>MOLECULE:</span> METHYLCOBALAMIN 1000 MCG INJ</li>
                         <li><span>FORM:</span> INJECTION  </li>
                         <li><span>THERAPY AREAS:</span> METHYLCOBALAMIN 1000 MCG INJ</li>

                         </ul>
                        
                     </div>
                  </div>

                  <div class="col-xs-12 col-sm-4 col-md-4 filtr-item" data-category="6" data-sort="">
                    <div class="filtr-item-block">
                        <img src="images/primeod.png" class="img-responsive" alt="PRIMECOBAL-1500 INJ"/>
                         <ul>
                         <li class="line-clamp"><span>MOLECULE:</span> METHYLCOBALAMIN 1500 MCG INJ</li>
                         <li><span>FORM:</span> INJECTION  </li>
                         <li><span>THERAPY AREAS:</span> VITAMIN B12 IN THE TREATMENT OF NEUROPATHY AND DEFICIENCY</li>

                         </ul>
                        
                     </div>
                  </div>

                  <div class="col-xs-12 col-sm-4 col-md-4 filtr-item" data-category="6" data-sort="">
                    <div class="filtr-item-block">
                        <img src="images/primecobal.png" class="img-responsive" alt="PRIMECOBAL-1500 INJ"/>
                         <ul>
                         <li class="line-clamp"><span>MOLECULE:</span> MECOBALAMIN1500MCG+VIT-B6-100MG+VI C-200MG+FOLIC ACID</li>
                         <li><span>FORM:</span> TABLET  </li>
                         <li><span>THERAPY AREAS:</span> MECOBALAMIN1500MCG+VIT-B6-100MG+VI C-200MG+FOLIC ACID</li>

                         </ul>
                        
                     </div>
                  </div>


                  <div class="col-xs-12 col-sm-4 col-md-4 filtr-item" data-category="6" data-sort="">
                    <div class="filtr-item-block">
                        <img src="images/setmaxv.png" class="img-responsive" alt="SETMAX-V Tablet"/>
                         <ul>
                         <li class="line-clamp"><span>MOLECULE:</span> MULTI VITAMINS & ESSENTIAL MINERALS TAB</li>
                         <li><span>FORM:</span> TABLET  </li>
                         <li><span>THERAPY AREAS:</span> MULTIVITAMIN AND MINERALS SUPPLEMENTATION</li>

                         </ul>
                        
                     </div>
                  </div>

                  <div class="col-xs-12 col-sm-4 col-md-4 filtr-item" data-category="6" data-sort="">
                    <div class="filtr-item-block">
                        <img src="images/statprime.png" class="img-responsive" alt="STATPRIME-10 TAB"/>
                         <ul>
                         <li class="line-clamp"><span>MOLECULE:</span> ATORVASTATIN CALCIUM 10 MG TAB</li>
                         <li><span>FORM:</span> TABLET  </li>
                         <li><span>THERAPY AREAS:</span> LIPID  REGULATOR</li>

                         </ul>
                        
                     </div>
                  </div>


                  <div class="col-xs-12 col-sm-4 col-md-4 filtr-item" data-category="6" data-sort="">
                    <div class="filtr-item-block">
                        <img src="images/statprime.png" class="img-responsive" alt="STATPRIME-20 TAB"/>
                         <ul>
                         <li class="line-clamp"><span>MOLECULE:</span> STATPRIME-20 TAB</li>
                         <li><span>FORM:</span> TABLET  </li>
                         <li><span>THERAPY AREAS:</span> LIPID  REGULATOR</li>

                         </ul>
                        
                     </div>
                  </div>


                  <div class="col-xs-12 col-sm-4 col-md-4 filtr-item" data-category="6" data-sort="">
                    <div class="filtr-item-block">
                        <img src="images/statprime.png" class="img-responsive" alt="STATPRIME-40"/>
                         <ul>
                         <li class="line-clamp"><span>MOLECULE:</span> ATORVASTATIN CALCIUM 40 MG TAB</li>
                         <li><span>FORM:</span> TABLET  </li>
                         <li><span>THERAPY AREAS:</span> LIPID  REGULATOR</li>

                         </ul>
                        
                     </div>
                  </div>


                  <div class="col-xs-12 col-sm-4 col-md-4 filtr-item" data-category="6" data-sort="">
                    <div class="filtr-item-block">
                        <img src="images/razorest.png" class="img-responsive" alt="RAZOREST-DSR"/>
                         <ul>
                         <li class="line-clamp"><span>MOLECULE:</span> RAZOREST-DSR</li>
                         <li><span>FORM:</span> TABLET  </li>
                         <li><span>THERAPY AREAS:</span> RAZOREST-DSR</li>

                         </ul>
                        
                     </div>
                  </div>

                  <div class="col-xs-12 col-sm-4 col-md-4 filtr-item" data-category="6" data-sort="">
                    <div class="filtr-item-block">
                        <img src="images/razorest.png" class="img-responsive" alt="RAZOREST"/>
                         <ul>
                         <li class="line-clamp"><span>MOLECULE:</span> RABEPRAZOLE GASTRO RESISTANT TABLETS</li>
                         <li><span>FORM:</span> TABLET  </li>
                         <li><span>THERAPY AREAS:</span> SUPERIOR ACTION IN GERD</li>

                         </ul>
                        
                     </div>
                  </div>


                  <div class="col-xs-12 col-sm-4 col-md-4 filtr-item" data-category="6" data-sort="">
                    <div class="filtr-item-block">
                        <img src="images/razorest.png" class="img-responsive" alt="RAZOREST-L"/>
                         <ul>
                         <li class="line-clamp" data-toggle="popover"  data-placement="top" title="MOLECULE" data-content=" RABEPRAZOLE SODIUM & LEVOSULPIRIDE SUSTAINED RELEASE CAPSULES"><span>MOLECULE:</span> RABEPRAZOLE SODIUM & LEVOSULPIRIDE SUSTAINED RELEASE CAPSULES</li>
                         <li><span>FORM:</span> TABLET  </li>
                         <li><span>THERAPY AREAS:</span> SUPERIOR COMBINATION IN GERD</li>

                         </ul>
                        
                     </div>
                  </div>


                  <div class="col-xs-12 col-sm-4 col-md-4 filtr-item" data-category="6" data-sort="">
                    <div class="filtr-item-block">
                        <img src="images/starusd3.png" class="img-responsive" alt="STARUS-D3"/>
                         <ul>
                         <li class="line-clamp"><span>MOLECULE:</span> CHOLECALCIFEROL TABLETS 60,000 I.U.</li>
                         <li><span>FORM:</span> TABLET  </li>
                         <li><span>THERAPY AREAS:</span> CHOLECALCIFEROL TABLETS 60,000 I.U.</li>

                         </ul>
                        
                     </div>
                  </div>


                  <div class="col-xs-12 col-sm-4 col-md-4 filtr-item" data-category="6" data-sort="">
                    <div class="filtr-item-block">
                        <img src="images/thiacrostabs.png" class="img-responsive" alt="THIACROS TAB"/>
                         <ul>
                         <li class="line-clamp"><span>MOLECULE:</span> THIAMINE 100 MG TABLETS IP</li>
                         <li><span>FORM:</span> TABLET  </li>
                         <li><span>THERAPY AREAS:</span> THIAMINE 100 MG TABLETS IP</li>

                         </ul>
                        
                     </div>
                  </div>


                  <div class="col-xs-12 col-sm-4 col-md-4 filtr-item" data-category="6" data-sort="">
                    <div class="filtr-item-block">
                        <img src="images/thiacros.png" class="img-responsive" alt="THIACROS INJ"/>
                         <ul>
                         <li class="line-clamp"><span>MOLECULE:</span> THIAMINE INJECTION I.P</li>
                         <li><span>FORM:</span> INJECTION  </li>
                         <li><span>THERAPY AREAS:</span> MEDICAL DIETRY SUPPLEMENTATION FOR FATIGUE AND ETC</li>

                         </ul>
                        
                     </div>
                  </div>

                  <div class="col-xs-12 col-sm-4 col-md-4 filtr-item" data-category="6" data-sort="">
                    <div class="filtr-item-block">
                        <img src="images/thiacrosgold.png" class="img-responsive" alt="THIACROS GOLD -INJ"/>
                         <ul>
                         <li class="line-clamp" data-toggle="popover"  data-placement="top" title="MOLECULE" data-content="EACH 2ML AMP CONTAINS METHYLCOBOLAMIN 1000 MCG.PYRIDOXINE HYDROCHLORIDE 100 MG.D-PANTHENAL 50MG.THIAMINE HYDROCHLORIDE 100 MG AND NIACINAMIDE 100MG FOR IM/IV USE"><span>MOLECULE:</span> EACH 2ML AMP CONTAINS METHYLCOBOLAMIN 1000 MCG.PYRIDOXINE HYDROCHLORIDE 100 MG.D-PANTHENAL 50MG.THIAMINE HYDROCHLORIDE 100 MG AND NIACINAMIDE 100MG FOR IM/IV USE</li>
                         <li><span>FORM:</span> INJECTION  </li>
                         <li><span>THERAPY AREAS:</span> MEDICAL DIETRY SUPPLEMENTATION FOR FATIGUE AND ETC</li>

                         </ul>
                        
                     </div>
                  </div>








                        
              
                </div>
       
      </div>
           
            

    </div>
  </section>
  
  <section class="action-band">
      <div class="container">
           <div class="row call-to-action fadeInUp wow">
               <div class="col-md-9">
                   <p>We are proud to have the opportunity to 
                       give you the Health of your dreams.</p>
               </div>
               <div class="col-md-3">
                   <a href="#" class="bnt btn-primary cust-btn-outline">Contact Us Now !</a>
               </div>
           </div>
      </div>
   
    </section>
    <?php include('footer.php'); ?>
    <script>
         $(document).ready(function(){
     var filterHeight=$('.filtr-container').height();
        $(function(){
             //Initialize filterizr with default options
             $('.filtr-container').filterizr();
       //$('.print-media-container').filterizr();
         });
     
         $(document).on('click', '.portfolio-nav ul li', function(){
           $('.portfolio-nav ul li').removeClass('active');
           $(this).addClass('active');
         });
    
    
   });
    </script>

<script>
      $(document).ready(function(){
          $('[data-toggle="popover"]').popover({ trigger: "hover" });
         
      });

      
      </script>