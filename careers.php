<?php include('header.php'); ?>
<section class="inner-banner-section fadeIn wow">
    <div class="solid-bg">
      <div class="container">
        <div class="banner-title fadeInDown wow" data-wow-delay=".2s">
          <h1>We Care About Your Life</h1>
          <p>To inspire hope and contribute to health and well-being by providing 
              the best care to every patient.</p>
          <div class="banner-breadcrumbs"> <span><a href="<?php echo $CONFIG_SERVER_ROOT;?>" class="home"><span>Home</span></a></span> <span class="sep">/</span> <span class="current">Careers</span> </div>
        </div>
      </div> <!-- Commit -->
    </div>
  </section>
  <section class="common-inner-block contact-block">
    <div class="container">
        <div class="row">
            <div class="col-md-12  section-title text-center fadeInDown wow" data-wow-delay=".4s" style=" margin-bottom:0px;">
            <h3 class="sectitle">Join Us</h3>
             <p>At Starus it is not just an employment. We nurture people, improve skills with professionally managed training programs, provide career progress and give continuous inputs for growth. Here people join due to clear differentiation in terms of every day challenges and focused incentive earnings. Starus club program is a reward through the hands of MD on an international platform every year for club qualified members. Join us to see the difference and for rewarding career.</p>  
            </div>
        </div>
       
        
            

    </div>
  </section>
  <section class="careers-block">
        <div class="container">
            <div class="row">
                    <div class="col-md-12 fadeInUp wow" data-wow-delay=".2s">
                            <div class="contact-form careers-frm">
                                    <?php 
                                      $message= $_SESSION['meassage'];
                                                echo $message;
                                                unset($_SESSION['meassage']); ?>
                                        <form class="" method="post" id="contactForm"  name="contactForm" action="ContactUsController.php">
                                          
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <h4>PERSONAL INFO</h4>
                                                </div>
                                
                                            </div>
                                                <div class="row">
                                                  <div class="col-md-4">
                                                    <div class="form-group">
                                                      <label for="Name">First Name</label>
                                                      <input type="text" class="form-control" placeholder="Enter First Name" name="first_name" id="first_name">
                                                    </div>
                                                  </div>
                                                  <div class="col-md-4">
                                                      <div class="form-group">
                                                        <label for="Name">Last Name</label>
                                                        <input type="text" class="form-control" placeholder="Enter Last Name" name="last_name" id="last_name">
                                                      </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                    <div class="form-group">
                                                          <label for="email">Your Email </label>
                                                          <input type="email" class="form-control" placeholder="Enter Your Email ID" name="email_address" id="email_address">
                                                        </div>
                                                    </div>
                                                  
                                                </div>
                                                <div class="row">
                                                <div class="col-md-12">
                                                    <h4>TECHNICAL INFO</h4>
                                                </div>
                                
                                            </div>
                                                <div class="row">
                                                  
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                          <label >Position Applying </label>
                                                          <input type="text" class="form-control" placeholder="Position Applying" name="position_applying" id="position_applying">
                                                        </div>
                                                      </div>
                                                  <div class="col-md-4">
                                                    <div class="form-group">
                                                      <label>Qualification</label>
                                                      <input type="text" class="form-control" placeholder="Qualification " name="qualification" id="qualification">
                                                    </div>
                                                  </div>
                                                  <div class="col-md-4">
                                                    <div class="form-group">
                                                      <label>Select Department</label>
                                                      <select class="form-control">
                                                      <option value="">--Select Department--</option>
                                                      <option value="sales">Sales</option>
                                                      <option value="sales">Marketing</option>
                                                      <option value="sales">Training</option>
                                                      <option value="sales">HRD</option>
                                                      </select>
                                                    </div>
                                                  </div>
                                                 
                                                </div>
                                                <div class="row">
                                                <div class="col-md-12">
                                                    <h4>UPLOAD FILES</h4>
                                                </div>
                                
                                            </div>
                                                <div class="row">
                                                  
                                                  
                                                </div>
                                                <div class="row">
                                                  <div class="col-md-6">
                                                    <div class="form-group">
                                                      <label>Resume</label>
                                                      <input type="file" class="form-control">
                                                     </div>
                                                  </div>
                                                  <div class="col-md-6">
                                                    <div class="form-group">
                                                      <label>Photograph</label>
                                                      <input type="file" class="form-control">
                                                     </div>
                                                  </div>
                                                </div>
                                                <div class="row">
                                                  <div class="col-md-6">
                                                    <button class="btn btn-primary" type="submit" class="Submit" name="Contact"  value="Contact Us">Send Message </button>
                                                   
                                                  </div>
                                                </div>
                                              </form>
                    
                                    </div>
                    </div>

                   
            </div>
        </div>

  </section>
  
    <?php include('footer.php'); ?>