<?php include('header.php'); ?>
<section class="inner-banner-section fadeIn wow">
    <div class="solid-bg">
      <div class="container">
        <div class="banner-title fadeInDown wow" data-wow-delay=".2s">
          <h1>We Care About Your Life</h1>
          <p>To inspire hope and contribute to health and well-being by providing 
              the best care to every patient.</p>
          <div class="banner-breadcrumbs"> <span><a href="<?php echo $CONFIG_SERVER_ROOT;?>" class="home"><span>Home</span></a></span> <span class="sep">/</span> <span class="current">Contact</span> </div>
        </div>
      </div>
    </div>
  </section>
  <section class="common-inner-block contact-block">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center fadeInDown wow" data-wow-delay=".4s">
             <p class="subtitle">If you would like to discuss a project with us, contact us today at any of the offices or drop a message here.</p>  
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 fadeInDown wow" data-wow-delay=".6s">
                <div class="contact-block-ui">
                        <h3> HR & Administration</h3>
                        <ul>
                            
                                <li> <strong>Phone: </strong> 9100141381</li>
                                <li> <strong>Email id: </strong> <span class="email">
                                        <a href="mailto:info@staruspharma.com">info@staruspharma.com</a>, <a href="mailto:hr@staruspharma.com">hr@staruspharma.com</a>
                                </span> </li>
                            </ul>
                </div>
            </div>
           <!-- <div class="col-md-4 fadeInDown wow" data-wow-delay=".8s">
                    <div class="contact-block-ui">
                            <h3> Finance Department</h3>
                            <ul>
                                
                                    <li> <strong>Phone: </strong> 040 2339 6234</li>
                                    <li> <strong>Mobile: </strong> +91 9581799949, 9959299557</li>
                                    <li> <strong>Email id: </strong> <span class="email">
                                            <a href="mailto:info@staruspharma.com">info@staruspharma.com</a>, <a href="mailto:hr@staruspharma.com">hr@staruspharma.com</a>
                                    </span> </li>
                                </ul>
                    </div>
                </div>
                <div class="col-md-4 fadeInDown wow" data-wow-delay="1s">
                        <div class="contact-block-ui">
                                <h3> Finance Department</h3>
                                <ul>
                                    
                                        <li> <strong>Phone: </strong> 040 2339 6234</li>
                                        <li> <strong>Mobile: </strong> +91 9581799949, 9959299557</li>
                                        <li> <strong>Email id: </strong> <span class="email">
                                                <a href="mailto:info@staruspharma.com">info@staruspharma.com</a>, <a href="mailto:hr@staruspharma.com">hr@staruspharma.com</a>
                                        </span> </li>
                                    </ul>
                        </div>
                    </div>
                    <div class="col-md-4 fadeInDown wow" data-wow-delay="1.2s">
                            <div class="contact-block-ui">
                                    <h3> Finance Department</h3>
                                    <ul>
                                        
                                            <li> <strong>Phone: </strong> 040 2339 6234</li>
                                            <li> <strong>Mobile: </strong> +91 9581799949, 9959299557</li>
                                            <li> <strong>Email id: </strong> <span class="email">
                                                    <a href="mailto:info@staruspharma.com">info@staruspharma.com</a>, <a href="mailto:hr@staruspharma.com">hr@staruspharma.com</a>
                                            </span> </li>
                                        </ul>
                            </div>
                        </div>
                        <div class="col-md-4 fadeInDown wow" data-wow-delay="1.4s">
                                <div class="contact-block-ui">
                                        <h3> Finance Department</h3>
                                        <ul>
                                            
                                                <li> <strong>Phone: </strong> 040 2339 6234</li>
                                                <li> <strong>Mobile: </strong> +91 9581799949, 9959299557</li>
                                                <li> <strong>Email id: </strong> <span class="email">
                                                        <a href="mailto:info@staruspharma.com">info@staruspharma.com</a>, <a href="mailto:hr@staruspharma.com">hr@staruspharma.com</a>
                                                </span> </li>
                                            </ul>
                                </div>
                            </div> -->
        </div>
        
            

    </div>
  </section>
  <section class="careers-block">
        <div class="container">
            <div class="row">
                    <div class="col-md-6 fadeInUp wow" data-wow-delay=".2s">
                            <div class="contact-form">
                                    <?php 
                                      $message= $_SESSION['meassage'];
                                                echo $message;
                                                unset($_SESSION['meassage']); ?>
                                        <form class="" method="post" id="contactForm"  name="contactForm" action="ContactUsController.php">
                                          
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <h3>Contact Us</h3>
                                                </div>
                                
                                            </div>
                                                <div class="row">
                                                  <div class="col-md-6">
                                                    <div class="form-group">
                                                      <label for="Name">First Name</label>
                                                      <input type="text" class="form-control" placeholder="Enter First Name" name="first_name" id="first_name">
                                                    </div>
                                                  </div>
                                                  <div class="col-md-6">
                                                      <div class="form-group">
                                                        <label for="Name">Last Name</label>
                                                        <input type="text" class="form-control" placeholder="Enter Last Name" name="last_name" id="last_name">
                                                      </div>
                                                    </div>
                                                  
                                                </div>
                                                <div class="row">
                                                  
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                          <label for="email">Your Email </label>
                                                          <input type="email" class="form-control" placeholder="Enter Your Email ID" name="email_address" id="email_address">
                                                        </div>
                                                      </div>
                                                  <div class="col-md-6">
                                                    <div class="form-group">
                                                      <label for="mobile">Contact Number</label>
                                                      <input type="text" class="form-control" placeholder="Enter Contact Number " name="contact_no" id="contact_no">
                                                    </div>
                                                  </div>
                                                 
                                                </div>
                                                <div class="row">
                                                  
                                                  
                                                </div>
                                                <div class="row">
                                                  <div class="col-md-12">
                                                    <div class="form-group">
                                                      <label for="compliant">Your Message</label>
                                                      <textarea class="form-control textarea" rows="3" placeholder="Your Message" name="contact_message" id="contact_message"></textarea>
                                                    </div>
                                                  </div>
                                                </div>
                                                <div class="row">
                                                  <div class="col-md-6">
                                                    <button class="btn btn-primary" type="submit" class="Submit" name="Contact"  value="Contact Us">Send Message </button>
                                                   
                                                  </div>
                                                </div>
                                              </form>
                    
                                    </div>
                    </div>

                    <div class="col-md-6 contact-info fadeInUp wow" data-wow-delay=".4s">
                            <h3>STARUS PHARMACEUTICALS PRIVATE LIMITED</h3>
                            <ul>
                                    <li><span class="icon"><i class="fa fa-map-marker" aria-hidden="true"></i></span>
                                        
                                    
                                       
                                        1st FLOOR, MOPARTHY TOWERS
LANE BESIDE KARACHI BAKERY
ROAD NO:1, BANJARA HILLS
HYDERABAD-500034.
                                    </li>
                                    <li><span class="icon"><i class="fa fa-phone" aria-hidden="true"></i></span>Phone:040 2339 6234</li>
                                    <li><span class="icon"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>Email: <a href="mailto:info@staruspharma.com">info@staruspharma.com</a></li>
                                    
                                  </ul>

                    </div>
            </div>
        </div>

  </section>
  
    <?php include('footer.php'); ?>