<?php
include_once('config.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>Starus Pharma </title>

<!-- Bootstrap -->
<link href="<?php echo $CONFIG_SERVER_ROOT;?>css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo $CONFIG_SERVER_ROOT;?>css/reset.css" rel="stylesheet">
<!-- Bootstrap bootstrap-touch-slider Slider Main Style Sheet -->
<link href="<?php echo $CONFIG_SERVER_ROOT;?>css/bootstrap-touch-slider.css" rel="stylesheet" media="all">
<!--  <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css" rel="stylesheet" media="all"> -->
<link rel="stylesheet" href="<?php echo $CONFIG_SERVER_ROOT;?>css/animate.min.css">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo $CONFIG_SERVER_ROOT;?>plugin/slick/slick.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $CONFIG_SERVER_ROOT;?>plugin/slick/slick-theme.css"/>

<link rel="stylesheet" type="text/css" href="<?php echo $CONFIG_SERVER_ROOT;?>plugin/source/jquery.fancybox.css?v=2.1.5" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo $CONFIG_SERVER_ROOT;?>plugin/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
	<!-- Add Thumbnail helper (this is optional) -->
	<link rel="stylesheet" type="text/css" href="<?php echo $CONFIG_SERVER_ROOT;?>plugin/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
  <style type="text/css">
		.fancybox-custom .fancybox-skin {
			box-shadow: 0 0 50px #222;
		}

		
	</style>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<link href="<?php echo $CONFIG_SERVER_ROOT;?>css/style.css" rel="stylesheet">
<link href="<?php echo $CONFIG_SERVER_ROOT;?>css/responsive.css" rel="stylesheet">
</head>
<body>
<div class="main-wrap">
  <!-- top small header bar -->
  <div class="header-bar-small">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <p>Welcome to Starus Pharma Pvt.Ltd</p>
            </div>
            <div class="col-md-9 text-right sm-nav-top">
                <ul>
                   
                    <li><i class="fa fa-phone" aria-hidden="true"></i> Call Us Now : <span>040 2339 6234 </span></li>
                    <li><i class="fa fa-paper-plane-o" aria-hidden="true"></i> Email : <a href="#"> info@staruspharma.com</a></li>
                    <li class="social-icon"><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a> <a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a> <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    <li><a href="http://www.staruspharma.com/webmail" target="_blank">Employee Login</a></li>
                    <li><a href="http://www.sanffa.info" target="_blank">E - Reporting</a></li>

                </ul>
            </div>

        </div>
    </div>
  </div>
  <!-- top small header bar End -->
  <?php 
  $home =  array('index');
  $products =  array('products');
  $about =  array('about');
  $stockist =  array('stockist');
  $partners =  array('partners');
  $careers =  array('careers');
  $contact =  array('contact');
  $gallery =  array('gallery');
  
  ?>

  <header class="header">
    <nav class="navbar navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar3"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
          <a class="navbar-brand" href="<?php echo $CONFIG_SERVER_ROOT;?>"> <img src="<?php echo $CONFIG_SERVER_ROOT;?>images/logo.png" alt="Logo"> </a> </div>
        <div id="navbar3" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li class="<?php if(in_array ($current_file_name, $home)) echo 'active'; ?>"><a href="<?php echo $CONFIG_SERVER_ROOT;?>">Home </a></li>
            <li class="<?php if(in_array ($current_file_name, $products)) echo 'active'; ?>"><a href="<?php echo $CONFIG_SERVER_ROOT;?>products">Products</a></li>
            <li class="<?php if(in_array ($current_file_name, $about)) echo 'active'; ?>"><a href="<?php echo $CONFIG_SERVER_ROOT;?>about">About</a></li>
            <li class="<?php if(in_array ($current_file_name, $stockist)) echo 'active'; ?>"><a href="<?php echo $CONFIG_SERVER_ROOT;?>stockist">Distribution Network</a></li>
             <li class="<?php if(in_array ($current_file_name, $careers)) echo 'active'; ?>"><a href="<?php echo $CONFIG_SERVER_ROOT;?>careers"> Careers </a></li>
             <li class="<?php if(in_array ($current_file_name, $gallery)) echo 'active'; ?>"><a href="<?php echo $CONFIG_SERVER_ROOT;?>gallery"> Gallery </a></li>
             
            <li class="<?php if(in_array ($current_file_name, $contact)) echo 'active'; ?>"><a href="<?php echo $CONFIG_SERVER_ROOT;?>contact">Contact</a></li>
             
          </ul>
        </div>
        <!--/.nav-collapse --> 
      </div>
      <!--/.container-fluid --> 
    </nav>
  </header>
  