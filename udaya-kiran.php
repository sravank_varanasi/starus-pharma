<?php include('header.php'); ?>
<section class="inner-banner-section fadeIn wow">
    <div class="solid-bg">
      <div class="container">
        <div class="banner-title fadeInDown wow" data-wow-delay=".2s">
          <h1>We Care About Your Life</h1>
          <p>To inspire hope and contribute to health and well-being by providing 
              the best care to every patient.</p>
          <div class="banner-breadcrumbs"> <span><a href="<?php echo $CONFIG_SERVER_ROOT;?>" class="home"><span>Home</span></a></span> <span class="sep">/</span> <span class="current">About</span> </div>
        </div>
      </div>
    </div>
  </section>
  <section class="team-section">
    <div class="container">
    <div class="row profile-view">
    	
        
    	<div class="col-md-3 hidden-xs">
        	
            <div class="prfile-img">
            	<img src="images/2-1-5-2-1.jpg" alt="Vincent Adams" class="img-responsive">
            </div>
            
        </div>
        <div class="col-md-9">
        	<h2>	Udaya Kiran Perla		</h2>
            <span class="role-view">Managing Director</span>
            
            <p>Mr Udaya Kiran is the founder of Starus Pharmaceuticals Pvt Ltd., and has extensive experience in sales and marketing in the Pharmaceutical Industry. Mr Uday is a Post Graduate in Biotechnology with M Phil in molecular biology and known for his Market knowledge.</p>
                 
                  <p>Mr Uday worked with M/s Sun Pharma as manager before becoming an entrepreneur and won several performances based awards and rewards.
                 </p>
                 <h3>BACHELOR DEEGRE</h3>
                  <p>Bachelor of Microbiology, Bio-Chemistry, Lab Technology from Andhra University.</p>
                  <h3>MASTER DEEGRE</h3>
                  <p>Master of Biotechnology, Microbiology, Bio-Chemistry from Barkatullah Viswavidyalaya.</p>
                  <h3>MASTER OF PHILOSOPHY:</h3>
                  <p>In Molecular Biology from Bharathi Dasan University.</p>

                 <div class="social-block">
                 	<ul class="social">
              <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
            </ul>
                 </div>
        	
        </div>
     
    	
    </div>
      
      
    </div>
  </section>

<?php include('footer.php'); ?>