<?php 

session_start();
include('class.phpmailer.php');
if(isset($_POST['Contact']) && $_POST['Contact'] == 'Contact Us'){
				$optionalFields = array();
				//User Registration Validation
				foreach ($_POST as $fieldname => $fieldValue) {
					if(!is_array($fieldValue)){
						$_POST[$fieldname] = trim($fieldValue);
					}
					if (empty($fieldValue) && !in_array($fieldname,$requiredFields)) {
						$fieldname = ucwords(strtolower(str_replace("_", " ", $fieldname)));
							$errMessage .= "<li>Please enter $fieldname</li>";
					}else {
						$_SESSION['ADD_USER'][$fieldname] = $fieldValue;
					}
				}
				if (!empty($errMessage)) {
					$_SESSION['meassage'] = $errMessage;
					header('Location:'.$_SERVER['HTTP_REFERER']);
				}
				
$name = $_POST['first_name'].' '.$_POST['last_name'];				
$to = "info@ssidcon.org";
//$to = "sravank.varanasi@gmail.com";
$subject = "Contact Request From ".$name;

$message = "
<html>
<head>
<title>Contact Request</title>
</head>
<body>
<p>Dear Administrator,</p>

<p>Contact requested by ".ucwords($name)."</p>
<table>
<tr>
<th>Name : </th>
<td>".ucwords($name)."</td>
</tr>
<tr>
<th>Email : </th>
<td>".$_POST['email_address']."</td>
</tr>
<tr>
<th>Phone : </th>
<td>".$_POST['contact_no']."</td>
</tr>
<tr>
<th>Message : </th>
<td>".$_POST['contact_message']."</td>
</tr>
</table>
</body>
</html>
";

// Always set content-type when sending HTML email

$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

// More headers
$headers .= 'From: <'.$_POST['email_address'].'>' . "\r\n";



if(mail($to,$subject,$message,$headers)){
	unset($_POST);
	unset($_SESSION['ADD_USER']);	
	$_SESSION['meassage']='<div class="alert alert-success alert-dismissable">
			  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			  <strong>Success!</strong> Contact Request Sent. Our team will get into touch with you soon.
			</div>';
	header('Location:'.$_SERVER['HTTP_REFERER']);
	exit(0);
}else {
	$_SESSION['meassage']='<div class="alert alert-danger alert-dismissable">
			  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			  <strong>Please Try Again.</strong>
			</div>';

	header('Location:'.$_SERVER['HTTP_REFERER']);
	exit(0);
}
} else if(isset($_POST['Grievance']) && $_POST['Grievance'] == 'Grievance Form'){

		
	$file_name = '';
		if(isset($_FILES['docs'])){
			$errors= array();
			$file_name = $_FILES['docs']['name'];
			$file_size = $_FILES['docs']['size'];
			$file_tmp = $_FILES['docs']['tmp_name'];
			$file_type = $_FILES['docs']['type'];
			$tmp = explode('.', $file_name);
			$file_ext = strtolower(end($tmp));

			$expensions= array("doc","docx","xls","xlsx");
			
			if(in_array($file_ext,$expensions)=== false){
			   $errors[]="extension not allowed, please choose a doc, docx, xls file.";
			}
			
			if($file_size > 2097152) {
			   $errors[]='File size must be excately 2 MB';
			}
			
			if(empty($errors)==true) {
			   move_uploaded_file($file_tmp,"uploads/".$file_name); //The folder where you would like your file to be saved
			  // echo "Success";
			}else{
			   $_SESSION['errors'] = '<div class="alert alert-danger">'.implode('<br/>',$errors).'</div>';
			   header('Location:'.$_SERVER['HTTP_REFERER']);
				exit(0);
			}
		 }




		$emailBody = "
<html>
<head>
<title>Contact Request</title>
</head>
<body>
<p>Dear Administrator,</p>

<p>".ucwords($name)." reqeust for support</p>
<table>
<tr>
<th>Name : </th>
<td>".ucwords($_POST['name'])."</td>
</tr>
<tr>
<th>Email : </th>
<td>".$_POST['email']."</td>
</tr>
<tr>
<th>Dp Id/ Folio No : </th>
<td>".$_POST['dpid']."</td>
</tr>
<tr>
<th>Location : </th>
<td>".$_POST['location']."</td>
</tr>
<tr>
<th>Phone : </th>
<td>".$_POST['contact']."</td>
</tr>
<tr>
<th>Message : </th>
<td>".$_POST['complaint']."</td>
</tr>

</table>
<p><b>Note: Requested from Website under Grievance Management.</b></p>
</body>
</html>
";

$emailSubject = 'Grievance request received from '.$_POST['name'].' via website';
		$mail = new PHPMailer(); // defaults to using php "mail()"
		$body = $emailBody;
		$mail->SetFrom($_POST['email'], $_POST['name']);
		$mail->Subject = $emailSubject;
		$mail->AltBody = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
		$mail->ClearAddresses();
		$mail->AddAddress('compliance@ssidcon.org');
		$mail->AddBcc('sravank.varanasi@gmail.com');
		$mail->MsgHTML($body);
		if(!empty($file_name)){
			$mail->addAttachment('uploads/'.$file_name);
		}
		if($mail->Send()){
			unset($_POST);
			unlink('uploads/'.$file_name);
				$_SESSION['meassage']='<div class="alert alert-success alert-dismissable">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						<strong>Success!</strong> Grievance Request Sent Successfully. Our team will get into touch with you soon.
						</div>';
				header('Location:'.$_SERVER['HTTP_REFERER']);
				exit(0);
		}else{
			return false;
		}

} else if(isset($_POST['Careers']) && $_POST['Careers'] == 'Careers Form'){

	$file_name = '';
		if(isset($_FILES['resume'])){
			$errors= array();
			$file_name = $_FILES['resume']['name'];
			$file_size = $_FILES['resume']['size'];
			$file_tmp = $_FILES['resume']['tmp_name'];
			$file_type = $_FILES['resume']['type'];
			$tmp = explode('.', $file_name);
			$file_ext = strtolower(end($tmp));

			$expensions= array("doc","docx","pdf");
			
			if(in_array($file_ext,$expensions)=== false){
			   $errors[]="extension not allowed, please choose a doc, docx, pdf file.";
			}
			
			if($file_size > 2097152) {
			   $errors[]='File size must be excately 2 MB';
			}
			
			if(empty($errors)==true) {
			   move_uploaded_file($file_tmp,"uploads/".$file_name); //The folder where you would like your file to be saved
			  // echo "Success";
			}else{
			   $_SESSION['errors'] = '<div class="alert alert-danger">'.implode('<br/>',$errors).'</div>';
			   header('Location:'.$_SERVER['HTTP_REFERER']);
				exit(0);
			}
		 }




		$emailBody = "
<html>
<head>
<title>Careers Form Request</title>
</head>
<body>
<p>Dear Administrator,</p>

<p>Careers request received from ".ucwords($name)."</p>
<table>
<tr>
<th>Name : </th>
<td>".ucwords($_POST['name'])."</td>
</tr>
<tr>
<th>Email : </th>
<td>".$_POST['email']."</td>
</tr>


<tr>
<th>Phone : </th>
<td>".$_POST['contact']."</td>
</tr>
<tr>
<th>Message : </th>
<td>".$_POST['message']."</td>
</tr>

</table>
<p><b>Note: Requested from Website under Careers.</b></p>
</body>
</html>
";

$emailSubject = 'Careers request received from '.$_POST['name'].' via website';
		$mail = new PHPMailer(); // defaults to using php "mail()"
		$body = $emailBody;
		$mail->SetFrom($_POST['email'], $_POST['name']);
		$mail->Subject = $emailSubject;
		$mail->AltBody = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
		$mail->ClearAddresses();
		$mail->AddAddress('raghu@ssidcon.org');
		$mail->AddBcc('sravank.varanasi@gmail.com');
		$mail->MsgHTML($body);
		if(!empty($file_name)){
			$mail->addAttachment('uploads/'.$file_name);
		}
		if($mail->Send()){
			unset($_POST);
			unlink('uploads/'.$file_name);
				$_SESSION['meassage']='<div class="alert alert-success alert-dismissable">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						<strong>Success!</strong> Your application submitted Successfully. Our team will get into touch with you soon.
						</div>';
				header('Location:'.$_SERVER['HTTP_REFERER']);
				exit(0);
		}else{
			return false;
		}

}
?>