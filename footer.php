<footer id="footer" class="site-footer">
    <div class="container">
      <div class="row">
        <div class="col-md-4 fadeInUp wow" data-wow-delay=".2s">
          <div class="foot-cont-block"> <a href="#"><img src="<?php echo $CONFIG_SERVER_ROOT;?>images/footer-logo.jpg" class="logo"></a>
            <p>Starus Pharmaceuticals Private Limited is a professionally managed progressive pharmaceutical marketing company headquartered at Hyderabad, India. </p>
          </div>
        </div>
        <div class="col-md-2 fadeInUp wow" data-wow-delay=".4s">
          <div class="foot-cont-block">
            <h3>Quick Links</h3>
            <ul>
            <li class="<?php if(in_array ($current_file_name, $home)) echo 'active'; ?>"><a href="<?php echo $CONFIG_SERVER_ROOT;?>">Home </a></li>
            <li class="<?php if(in_array ($current_file_name, $products)) echo 'active'; ?>"><a href="<?php echo $CONFIG_SERVER_ROOT;?>products">Products</a></li>
            <li class="<?php if(in_array ($current_file_name, $about)) echo 'active'; ?>"><a href="<?php echo $CONFIG_SERVER_ROOT;?>about">About</a></li>
            <li class="<?php if(in_array ($current_file_name, $stockist)) echo 'active'; ?>"><a href="<?php echo $CONFIG_SERVER_ROOT;?>stockist">Stockist</a></li>
             <li class="<?php if(in_array ($current_file_name, $careers)) echo 'active'; ?>"><a href="<?php echo $CONFIG_SERVER_ROOT;?>careers"> Careers </a></li>
            <li class="<?php if(in_array ($current_file_name, $contact)) echo 'active'; ?>"><a href="<?php echo $CONFIG_SERVER_ROOT;?>contact">Contact</a></li>
           

            </ul>
          </div>
        </div>
        <div class="col-md-3 fadeInUp wow" data-wow-delay=".6s">
          <div class="foot-cont-block">
            <h3>Contact Us </h3>
            <p>1st FLOOR, MOPARTHY TOWERS,<br>
                LANE BESIDE KARACHI BAKERY,<br>
                ROAD NO:1, BANJARA HILLS,<br>
                HYDERABAD-500034, INDIA</p>
          </div>
        </div>
        <div class="col-md-3 fadeInUp wow" data-wow-delay=".8s">
          <div class="foot-cont-block">
            <h3>Quick contact</h3>
            <ul class="cont-list">
              
              <li><i class="fa fa-phone" aria-hidden="true"></i>040 49549999</li>
             
              <li><i class="fa fa-envelope-o" aria-hidden="true"></i> info@staruspharma.com</li>
              <li><i class="fa fa-envelope-o" aria-hidden="true"></i> hr@staruspharma.com</li>
            </ul>
            
          </div>
        </div>
      </div>
    </div>
  </footer>
  <div class="fixed-footer">Copyright ©2018 Starus Pharma Pvt.Ltd. All Rights Reserved</div>
</div>
<a href="#home" class="scrollup" style="display: block;"><i class="fa fa-angle-up fa-3x"></i></a>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> 
<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script src="<?php echo $CONFIG_SERVER_ROOT;?>js/bootstrap.min.js"></script> 
<script type="text/javascript" src="<?php echo $CONFIG_SERVER_ROOT;?>plugin/slick/slick.min.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.touchswipe/1.6.18/jquery.touchSwipe.min.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script> 
<!-- Bootstrap bootstrap-touch-slider Slider Main JS File --> 
<script src="<?php echo $CONFIG_SERVER_ROOT;?>js/bootstrap-touch-slider.js"></script> 
<script src="<?php echo $CONFIG_SERVER_ROOT;?>js/wow.min.js"></script> 
<script src="http://www.rpwebapps.com/js/jquery.filterizr.js"></script> 

  <!-- Add mousewheel plugin (this is optional) -->
  <script type="text/javascript" src="<?php echo $CONFIG_SERVER_ROOT;?>plugin/source/jquery.mousewheel.pack.js?v=3.1.3"></script>
  

	<script type="text/javascript" src="<?php echo $CONFIG_SERVER_ROOT;?>plugin/source/jquery.fancybox.pack.js?v=2.1.5"></script>
	

	<!-- Add Button helper (this is optional) -->
	
	<script type="text/javascript" src="<?php echo $CONFIG_SERVER_ROOT;?>plugin/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>

	<!-- Add Thumbnail helper (this is optional) -->
	<script type="text/javascript" src="<?php echo $CONFIG_SERVER_ROOT;?>plugin/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>

	<!-- Add Media helper (this is optional) -->
  <script type="text/javascript" src="<?php echo $CONFIG_SERVER_ROOT;?>plugin/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
  <script type="text/javascript">
		$(document).ready(function() {
			/*
			 *  Simple image gallery. Uses default settings
			 */

			$('.fancybox').fancybox();

			/*
			 *  Different effects
			 */

			// Change title type, overlay closing speed
			$(".fancybox-effects-a").fancybox({
				helpers: {
					title : {
						type : 'outside'
					},
					overlay : {
						speedOut : 0
					}
				}
			});

			// Disable opening and closing animations, change title type
			$(".fancybox-effects-b").fancybox({
				openEffect  : 'none',
				closeEffect	: 'none',

				helpers : {
					title : {
						type : 'over'
					}
				}
			});

			// Set custom style, close if clicked, change title type and overlay color
			$(".fancybox-effects-c").fancybox({
				wrapCSS    : 'fancybox-custom',
				closeClick : true,

				openEffect : 'none',

				helpers : {
					title : {
						type : 'inside'
					},
					overlay : {
						css : {
							'background' : 'rgba(238,238,238,0.85)'
						}
					}
				}
			});

			// Remove padding, set opening and closing animations, close if clicked and disable overlay
		

			/*
			 *  Button helper. Disable animations, hide close button, change title type and content
			 */

			$('.fancybox-buttons').fancybox({
				openEffect  : 'none',
				closeEffect : 'none',

				prevEffect : 'none',
				nextEffect : 'none',

				closeBtn  : false,

				helpers : {
					title : {
						type : 'inside'
					},
					buttons	: {}
				},

				afterLoad : function() {
					this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
				}
			});



				$("#fancybox-manual-bglr").click(function() {
				$.fancybox.open([
					{
						href : 'images/gallery/cyclemeetingjuly/1.jpg',
						title : 'Cycle Meeting July 2018@Bangalore'
					},{
						href : 'images/gallery/cyclemeetingjuly/2.jpg',
						title : 'Cycle Meeting July 2018@Bangalore'
					},
          {
						href : 'images/gallery/cyclemeetingjuly/3.jpg',
						title : 'Cycle Meeting July 2018@Bangalore'
					}], {
					helpers : {
						thumbs : {
							width: 75,
							height: 50
						}
					}
				});
			});





			$("#fancybox-manual-mumbai").click(function() {
				$.fancybox.open([
					{
						href : 'images/gallery/cyclemeetingjuly/IMG_6933.JPG',
						title : 'Budget Meeting 2018@Mumbai'
					},{
						href : 'images/gallery/cyclemeetingjuly/IMG_6820.JPG',
						title : 'Budget Meeting 2018@Mumbai'
					},
          {
						href : 'images/gallery/cyclemeetingjuly/IMG_6812.JPG',
						title : 'Budget Meeting 2018@Mumbai'
					},
          {
						href : 'images/gallery/cyclemeetingjuly/IMG_6809.JPG',
						title : 'Budget Meeting 2018@Mumbai'
					},
          {
						href : 'images/gallery/cyclemeetingjuly/IMG_6801.JPG',
						title : 'Budget Meeting 2018@Mumbai'
					},
          {
						href : 'images/gallery/cyclemeetingjuly/IMG_6792.JPG',
						title : 'Budget Meeting 2018@Mumbai'
					},
          {
						href : 'images/gallery/cyclemeetingjuly/IMG_6791.JPG',
						title : 'Budget Meeting 2018@Mumbai'
					},
          {
						href : 'images/gallery/cyclemeetingjuly/IMG_6790.JPG',
						title : 'Budget Meeting 2018@Mumbai'
					},
          {
						href : 'images/gallery/cyclemeetingjuly/IMG_6789.JPG',
						title : 'Budget Meeting 2018@Mumbai'
					},
          {
						href : 'images/gallery/cyclemeetingjuly/IMG_6782.JPG',
						title : 'Budget Meeting 2018@Mumbai'
					},
          {
						href : 'images/gallery/cyclemeetingjuly/IMG_6781.JPG',
						title : 'Budget Meeting 2018@Mumbai'
					}], {
					helpers : {
						thumbs : {
							width: 75,
							height: 50
						}
					}
				});
			});


		});
	</script>

<script type="text/javascript">
         $('#bootstrap-touch-slider').bsTouchSlider();

         document.onscroll = function() {
    if( $(window).scrollTop() > $('header').height() ) {
        $('.header').addClass('navbar-small');
    }
    else {
        //$('nav > div.navbar').removeClass('navbar-fixed-top').addClass('navbar-static-top');
        $('.header').removeClass('navbar-small');
    }
};


     </script> 
<script type="text/javascript">
        $(document).ready(function() {
          $('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
            disableOn: 700,
            type: 'iframe',
            mainClass: 'mfp-fade',
            removalDelay: 160,
            preloader: false,
  
            fixedContentPos: false
          });


          $('.prlist-slide').slick({
  dots: false,
  infinite: false,
  speed: 300,
  slidesToShow: 2,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});

(function($) {
    "use strict"; // Start of use strict
    // Initialize WOW.js Scrolling Animations
    new WOW().init();

})(jQuery); // End of use strict

//--------------------Creative.js--animation-----------------------


        });
		$(window).scroll(function(){
		if ($(this).scrollTop() > 100) {
			$('.scrollup').fadeIn();
			} else {
				$('.scrollup').fadeOut();
			}
		});
		$('.scrollup').click(function(){
			$("html, body").animate({ scrollTop: 0 }, 1000);
				return false;
		});
      </script>

<script type="text/javascript">
	 	


    function toggleIcon(e) {
    $(e.target)
        .prev('.panel-heading')
        .find(".more-less")
        .toggleClass('glyphicon-plus glyphicon-minus');
}
$('.panel-group').on('hidden.bs.collapse', toggleIcon);
$('.panel-group').on('shown.bs.collapse', toggleIcon);
   
  </script>
</body>
</html>