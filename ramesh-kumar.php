<?php include('header.php'); ?>
<section class="inner-banner-section fadeIn wow">
    <div class="solid-bg">
      <div class="container">
        <div class="banner-title fadeInDown wow" data-wow-delay=".2s">
          <h1>We Care About Your Life</h1>
          <p>To inspire hope and contribute to health and well-being by providing 
              the best care to every patient.</p>
          <div class="banner-breadcrumbs"> <span><a href="<?php echo $CONFIG_SERVER_ROOT;?>" class="home"><span>Home</span></a></span> <span class="sep">/</span> <span class="current">About</span> </div>
        </div>
      </div>
    </div>
  </section>
  <section class="team-section">
    <div class="container">
    <div class="row profile-view">
    	
        
    	<div class="col-md-3  hidden-xs">
        	
            <div class="prfile-img">
            	<img src="images/2-1-5-2-1.jpg" alt="Vincent Adams" class="img-responsive">
            </div>
            
        </div>
        <div class="col-md-9">
        	<h2>M Ramesh Kumar	</h2>
            <span class="role-view">Managing Director</span>
            
            <p>
Mr Ramesh Kumar, has extensive experience of 3 decades in sales and marketing with India’s leading pharmaceutical companies including Gufic, Torrent and Sun Pharma. Mr Ramesh’s previous assignments includes heading national sales and marketing of Sun Pharma’s Gynecology and Neuro-psychiatry divisions. Mr Ramesh Kumar is a Gold Medalist in Zoology from Andhra University and has master’s degree in marketing from Pondicherry Central university apart from a post-graduation in management and new venture creation from India’s premier management institute SP Jain Institute of Management and Research Mumbai.</p>
                 
                  <p>Mr Ramesh Kumar won several performances based awards throughout career and has extensive knowledge in the areas of sales, marketing and training.</p>
                <p>Mr Ramesh Kumar known for bringing in newer molecules in medical food category into Indian market and few examples includes Cerebroprotein Hydrolysate oral formulation and L-theanine / Melatonin as a combination etc.,</p>
                  <h3>BACHELOR DEGREE</h3>
                  <p>Mr Ramesh Kumar is a Gold Medalist in Zoology from Andhra University.</p>
                  <h3>MASTER DEGREE</h3>
                  <p>master’s degree in marketing from Pondicherry Central university apart from a post-graduation in management,new venture creation from India’s premier management institute SP Jain Institute of Management and Research Mumbai.</p>

                 <div class="social-block">
                 	<ul class="social">
              <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
            </ul>
                 </div>
        	
        </div>
     
    	
    </div>
      
      
    </div>
  </section>

<?php include('footer.php'); ?>